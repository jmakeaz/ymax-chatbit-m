<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" %>
<html lang="ko">

<head>
    <title> CHATBIT - YMAX </title>
    <meta charset="utf-8" />
    <meta name="format-detection" content="telephone=no" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <link href="/chatbit_m/css/base.css" type="text/css" rel="stylesheet" />
    <link href="/chatbit_m/css/layout.css" type="text/css" rel="stylesheet" />
    <link href="/chatbit_m/css/common.css" type="text/css" rel="stylesheet" />
    <link href="/chatbit_m/css/member.css" type="text/css" rel="stylesheet" />
    <script src="/chatbit/js/1.10.1.jquery.min.js" type="text/javascript" charset="utf-8"></script>
</head>

<body>

    <jsp:include page="/chatbit_m/include/header.jsp" flush="true"></jsp:include>

    <div class="containerWrap">

        <h2 class="pageTitle">회원탈퇴</h2>

        <p class="memberOut">
            <strong class="memberOutTitle">회원탈퇴가 완료되었습니다.</strong>
            <span class="memberOutText">
                언제나 노력하는 <strong>Chatbit</strong>가 되어<br />
                다시 만날 수 있기를 바랍니다.<br />
                그동안 <strong>Chatbit</strong>를 이용해 주셔서 감사합니다.
            </span>
            <a href="/chatbit_m/" class="cBtn">메인으로 이동</a>
        </p>

    </div>

    <jsp:include page="/chatbit_m/include/footer.jsp" flush="true"></jsp:include>

</body>

</html>