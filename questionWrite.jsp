<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" %>
<html lang="ko">

<head>
    <title> CHATBIT - YMAX </title>
    <meta charset="utf-8" />
    <meta name="format-detection" content="telephone=no" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <link href="/chatbit_m/css/base.css" type="text/css" rel="stylesheet" />
    <link href="/chatbit_m/css/layout.css" type="text/css" rel="stylesheet" />
    <link href="/chatbit_m/css/common.css" type="text/css" rel="stylesheet" />
    <link href="/chatbit_m/css/board.css" type="text/css" rel="stylesheet" />
    <script src="/chatbit/js/1.10.1.jquery.min.js" type="text/javascript" charset="utf-8"></script>
</head>

<body>

    <jsp:include page="/chatbit_m/include/header.jsp" flush="true"></jsp:include>

    <div class="containerWrap">

        <jsp:include page="/chatbit_m/include/customerMenu.jsp" flush="true"></jsp:include>
        <script>
            $('.jsBoardLinks [data-value="2"]').trigger('click');
        </script>

        <p class="questionNotice2">
            이용 중 문제가 발생하거나 궁금하신 점이 있을 때
            1:1 문의를 보내주시면 확인 후 답변 드리겠습니다.
            답변 내용은 <a href="./question.jsp" class="colorPrimary">문의하기 목록</a>에서 확인해 주세요.
        </p>

        <form action="#">
            <fieldset class="questionWrite">
                <p class="inputField writeTitle">
                    <label for="tokTitle" class="visuallyhidden">제목</label>
                    <span class="input">
                        <input type="text" id="tokTitle" value="" placeholder="제목을 입력해 주세요.">
                    </span>
                    <strong class="message">사용할 수 없습니다.</strong>
                </p>

                <div class="writeBodyWrap jsEditor">
                    <div class="writeBody">
                        <textarea class="editor"></textarea>
                    </div>
                    <div class="editorPlaceholder">
                        <p>정확한 상담을 위해 문의 내용을 자세히 작성해 주세요. 캡쳐 또는 사진 등을 첨부해 주시면 상담에 많은 도움이 됩니다.</p>
                    </div>
                </div>

                <div class="writeBodyMeta">
                    <span class="imgUpload cBtn2">
                        <button type="button" class="figment">
                            <span class="iconPhoto"></span>이미지 첨부
                        </button>
                        <input type="file" accept="image/*" class="uploadInputFile">
                    </span>

                    <span class="writeEditorLength">0/4000</span>
                </div>

                <div class="writeButtons rowGroup">
                    <button type="button" class="cBtn line">취소</button>
                    <button class="cBtn">문의접수</button>
                </div>
            </fieldset>
        </form>
    </div>

    <jsp:include page="/chatbit_m/include/footer.jsp" flush="true"></jsp:include>

</body>

</html>