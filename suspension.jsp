<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" %>
<html lang="ko">

<head>
    <title> CHATBIT - YMAX </title>
    <meta charset="utf-8" />
    <meta name="format-detection" content="telephone=no" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <link href="/chatbit_m/css/base.css" type="text/css" rel="stylesheet" />
    <link href="/chatbit_m/css/layout.css" type="text/css" rel="stylesheet" />
    <link href="/chatbit_m/css/common.css" type="text/css" rel="stylesheet" />
    <script src="/chatbit/js/1.10.1.jquery.min.js" type="text/javascript" charset="utf-8"></script>
</head>

<body>

    <jsp:include page="/chatbit_m/include/header.jsp" flush="true"></jsp:include>

    <div class="containerWrap">

        <p>이용 정지 안내 팝업을 보여주기 위한 임시 페이지</p>

    </div>

    <jsp:include page="/chatbit_m/include/footer.jsp" flush="true"></jsp:include>
    <script>
        $(document).ready(function () {
            dimOn();

            // 이용 정지 안내 팝업 호출
            $('.jsNoticePopup').fadeIn('fast');
        });
    </script>

</body>

</html>