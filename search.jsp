<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" %>
<html lang="ko">

<head>
    <title> CHATBIT - YMAX </title>
    <meta charset="utf-8" />
    <meta name="format-detection" content="telephone=no" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <link href="/chatbit_m/css/base.css" type="text/css" rel="stylesheet" />
    <link href="/chatbit_m/css/layout.css" type="text/css" rel="stylesheet" />
    <link href="/chatbit_m/css/common.css" type="text/css" rel="stylesheet" />
    <script src="/chatbit/js/1.10.1.jquery.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="/chatbit_m/js/jquery.dotdotdot.js" type="text/javascript" charset="utf-8"></script>
</head>

<body>

    <jsp:include page="/chatbit_m/include/header.jsp" flush="true"></jsp:include>

    <div class="containerWrap">
        <!-- 결과 없을 때 -->
        <div class="searchHeading noResult">
            <h2>
                <strong class="searchWord">‘ㅋㅗ인’</strong>
                에 대한<br />
                검색결과가 없습니다.
            </h2>
            <p class="searchRetry">
                입력하신 검색어와 일치하는 결과가 없습니다.<br>
                다시 한번 확인해주세요.
            </p>
        </div>
        <!-- // 결과 없을 때 -->

        <div class="searchHeading">
            <h2>
                <strong class="searchWord">‘ㅋㅗ인’</strong>
                에 대한 검색결과입니다.
            </h2>
        </div>

        <div class="searchResult">
            <div class="searchedCategory">
                <h2 class="text">코인뉴스</h2>
                <a href="/chatbit_m/searchDetail.jsp" class="more">더보기</a>
            </div>
            <ul class="searchResultList">
                <li>
                    <a href="#">
                        이더리움 공동설립자 조셉 루빈 "최근 암호 화폐 가격 하락세도 산업 성장 막지 못해"
                    </a>
                </li>
                <li>
                    <a href="#">
                        가상화폐 시세 대체로 오름세, 테마기업
                    </a>
                </li>
                <li>
                    <a href="#">
                        블록체인 가상화폐 기술교류..다빈치 글로벌 암호화폐 콘퍼런스 2018 성료
                    </a>
                </li>
                <li>
                    <a href="#">
                        [가상화폐 입법추진 1년] ① 끊임없이 흔들린 거래소 보안… 투자자 여전히 ‘전전긍긍‘
                    </a>
                </li>
                <li>
                    <a href="#">
                        가상화폐 시세 대체로 오름세, 테마기업 주가는 혼조세
                    </a>
                </li>
            </ul>
            <div class="searchedCategory">
                <h2 class="text">코인톡톡</h2>
                <a href="/chatbit_m/searchDetail.jsp" class="more">더보기</a>
            </div>
            <div class="searchedCategory">
                <h2 class="text">수익인증</h2>
                <a href="/chatbit_m/searchDetail.jsp" class="more">더보기</a>
            </div>
            <ul class="searchResultList">
                <li>
                    <a href="#">
                        올스타빗 스케치 오늘만 코인 2배 됐네요.
                    </a>
                </li>
                <li>
                    <a href="#">
                        코인베이스, 암호화폐 인덱스 펀드 수수료
                        인하에도 시세 하락세
                    </a>
                </li>
                <li>
                    <a href="#">
                        수익을 챙기는 코인거래법!
                    </a>
                </li>
            </ul>
            <div class="searchedCategory">
                <h2 class="text">투자일기</h2>
                <a href="/chatbit_m/searchDetail.jsp" class="more">더보기</a>
            </div>
            <div class="searchedCategory">
                <h2 class="text">분석게시판</h2>
                <a href="/chatbit_m/searchDetail.jsp" class="more">더보기</a>
            </div>
            <div class="searchedCategory">
                <h2 class="text">자유게시판</h2>
                <a href="/chatbit_m/searchDetail.jsp" class="more">더보기</a>
            </div>
            <div class="searchedCategory">
                <h2 class="text">코인프로</h2>
                <a href="/chatbit_m/searchDetail.jsp" class="more">더보기</a>
            </div>
            <div class="searchedCategory">
                <h2 class="text">채팅</h2>
                <a href="/chatbit_m/searchDetail.jsp" class="more">더보기</a>
            </div>
            <ul class="searchResultList">
                <li>
                    <a href="#">
                        8월 터질 코인들 대공개!
                    </a>
                </li>
                <li>
                    <a href="#">
                        수익을 챙기는 코인거래법!
                    </a>
                </li>
            </ul>
        </div>

    </div>

    <jsp:include page="/chatbit_m/include/footer.jsp" flush="true"></jsp:include>
    <script>
        // 기사글 제목 말줄임표
        $(function () {
            $('.ellipsis_label').each(function (i, element) {
                $(element).dotdotdot({
                    keep: '.labelWrap'
                });
            });
        });
    </script>

</body>

</html>