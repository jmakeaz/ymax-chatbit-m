<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" %>
<html lang="ko">
<head>
<title> CHATBIT - YMAX </title>
<meta charset="utf-8" />
<meta name="format-detection" content="telephone=no" />
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<script src="/chatbit/js/1.10.1.jquery.min.js" type="text/javascript" charset="utf-8"></script>
<link href="/chatbit_m/css/base.css" type="text/css" rel="stylesheet"  />
<link href="/chatbit_m/css/layout.css" type="text/css" rel="stylesheet"  />
<link href="/chatbit_m/css/common.css" type="text/css" rel="stylesheet"  />
<link href="/chatbit_m/css/slideTab.css" type="text/css" rel="stylesheet"  />
<link href="/chatbit_m/css/main.css" type="text/css" rel="stylesheet"  />
</head>
<body>

<jsp:include page="/chatbit_m/include/header.jsp" flush="true"></jsp:include>
<hr class="h9"/>
<div class="containerWrap">
	<div class="coinPriceTop">
		<strong>
			코인 시세표
                <a href="javascript:;" class="coinSettingBt"><img src="/chatbit_m/img/svg/coin_set.svg" alt="" width="16" /></a>
		</strong>
		<a href="javascript:;" class="coinPriceBt"></a>
	</div>
	<div class="coinPriceWrap">
		<div class="coinPrice swiper-container">
			<ul class="coinPriceTab swiper-wrapper">
				<li data-tab="BTC" class="swiper-slide on"><a href="#">BTC</a></li>
				<li data-tab="BCH" class="swiper-slide"><a href="#">BCH</a></li>
				<li data-tab="ETH" class="swiper-slide"><a href="#">ETH</a></li>
				<li data-tab="ETC" class="swiper-slide"><a href="#">ETC</a></li>
				<li data-tab="XRP" class="swiper-slide"><a href="#">XRP</a></li>
				<li data-tab="EOS" class="swiper-slide"><a href="#">EOS</a></li>
				<li data-tab="LTC" class="swiper-slide"><a href="#">LTC</a></li>
				<li data-tab="QTUM" class="swiper-slide"><a href="#">QTUM</a></li>
				<li data-tab="BTB" class="swiper-slide"><a href="#">BTB</a></li>
				<li data-tab="BTS" class="swiper-slide"><a href="#">BTS</a></li>
			</ul>
		</div>
		<div class="coinPriceCont">
			<div class="cpc_BTC" style="display:block;">
				<table>
					<thead>
						<tr>
							<th scope="col">거래소</th>
							<th scope="col">실시간</th>
							<th scope="col">24시간 변동률(%)</th>
							<th scope="col">프리미엄</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>빗썸</td>
							<td>7,178,000원</td>
							<td><strong class="down">-107,000(-2.08%)</strong></td>
							<td class="primeum">+2.18%</td>
						</tr>
						<tr>
							<td>업비트</td>
							<td>7,178,000원</td>
							<td><strong class="up">+5,000(+1.11%)</strong></td>
							<td class="primeum">+2.18%</td>
						</tr>
						<tr>
							<td>코인원</td>
							<td>7,178,000원</td>
							<td><strong class="down">-107,000(-2.08%)</strong></td>
							<td class="primeum">+2.18%</td>
						</tr>
						<tr>
							<td>코빗</td>
							<td>7,178,000원</td>
							<td><strong class="down">-107,000(-2.08%)</strong></td>
							<td class="primeum">+2.18%</td>
						</tr>
						<tr>
							<td>플라이어</td>
							<td>7,178,000원</td>
							<td><strong class="down">-107,000(-2.08%)</strong></td>
							<td class="primeum">+2.18%</td>
						</tr>
						<tr>
							<td>바이낸스</td>
							<td>7,178,000원</td>
							<td><strong class="down">-107,000(-2.08%)</strong></td>
							<td class="primeum">+2.18%</td>
						</tr>
					</tbody>
				</table>
				
			</div>
			<div class="cpc_BCH">
				2
			</div>
			<div class="cpc_ETH">
				3
			</div>
			<div class="cpc_ETC">
				4
			</div>
			<div class="cpc_XRP">
				5
			</div>
			<div class="cpc_EOS">
				6
			</div>
			<div class="cpc_LTC">
				7
			</div>
			<div class="cpc_QTUM">
				8
			</div>
			<div class="cpc_BTB">
				9
			</div>
			<div class="cpc_BTS">
				10
			</div>
		</div>
	</div>
	<hr class="h9"/>
	<div class="coinSetting">
		<p class="tit">
			<strong>암호화폐 설정</strong>
			<span>관심화폐를 선택해 주세요. (최대 10개)</span>
			<a href="#" class="coinSettingClose"></a>
		</p>
		<div class="coinSettingList">
			<ul>
				<li><a href="#" class="on"><strong>BTC</strong>비트코인</a></li>
				<li><a href="#"><strong>ETH</strong>이더리움</a></li>
				<li><a href="#"><strong>LTC</strong>라이트코인</a></li>
				<li><a href="#"><strong>BCH</strong>비트코인 캐시</a></li>
				<li><a href="#" class="on"><strong>XMR</strong>모네로</a></li>
				<li><a href="#"><strong>QTUM</strong>퀀텀</a></li>
				<li><a href="#"><strong>BTG</strong>비트코인 골드</a></li>
				<li><a href="#"><strong>DASH</strong>대시</a></li>
				<li><a href="#"><strong>ZEC</strong>지캐시</a></li>
				<li><a href="#"><strong>BTC</strong>비트코인</a></li>
				<li><a href="#"><strong>ETH</strong>이더리움</a></li>
				<li><a href="#"><strong>LTC</strong>라이트코인</a></li>
				<li><a href="#"><strong>BCH</strong>비트코인 캐시</a></li>
				<li><a href="#"><strong>XMR</strong>모네로</a></li>
				<li><a href="#"><strong>QTUM</strong>퀀텀</a></li>
				<li><a href="#"><strong>BTG</strong>비트코인 골드</a></li>
				<li><a href="#"><strong>DASH</strong>대시</a></li>
				<li><a href="#" class="on"><strong>ZEC</strong>지캐시</a></li>
				<li><a href="#"><strong>BTC</strong>비트코인</a></li>
				<li><a href="#"><strong>ETH</strong>이더리움</a></li>
				<li><a href="#"><strong>LTC</strong>라이트코인</a></li>
				<li><a href="#"><strong>BCH</strong>비트코인 캐시</a></li>
				<li><a href="#"><strong>XMR</strong>모네로</a></li>
				<li><a href="#"><strong>QTUM</strong>퀀텀</a></li>
				<li><a href="#"><strong>BTG</strong>비트코인 골드</a></li>
				<li><a href="#"><strong>DASH</strong>대시</a></li>
				<li><a href="#"><strong>ZEC</strong>지캐시</a></li>
				<li><a href="#"><strong>BTC</strong>비트코인</a></li>
				<li><a href="#"><strong>ETH</strong>이더리움</a></li>
				<li><a href="#"><strong>LTC</strong>라이트코인</a></li>
				<li><a href="#"><strong>BCH</strong>비트코인 캐시</a></li>
				<li><a href="#"><strong>XMR</strong>모네로</a></li>
				<li><a href="#"><strong>QTUM</strong>퀀텀</a></li>
				<li><a href="#"><strong>BTG</strong>비트코인 골드</a></li>
				<li><a href="#"><strong>DASH</strong>대시</a></li>
				<li><a href="#"><strong>ZEC</strong>지캐시</a></li>
				<li><a href="#"><strong>BTC</strong>비트코인</a></li>
				<li><a href="#"><strong>ETH</strong>이더리움</a></li>
				<li><a href="#"><strong>LTC</strong>라이트코인</a></li>
				<li><a href="#"><strong>BCH</strong>비트코인 캐시</a></li>
				<li><a href="#"><strong>XMR</strong>모네로</a></li>
				<li><a href="#"><strong>QTUM</strong>퀀텀</a></li>
				<li><a href="#"><strong>BTG</strong>비트코인 골드</a></li>
				<li><a href="#"><strong>DASH</strong>대시</a></li>
				<li><a href="#"><strong>ZEC</strong>지캐시</a></li>
			</ul>
		</div>
		<a href="#" class="confirm">확인</a>
	</div>

	<div class="main_section">
		<p class="tit">
			<strong>코인뉴스</strong>
			<a href="#"></a>
		</p>
		<div class="boardTop">
			<a href="#">
				<div class="newsImg"><img src="/chatbit_m/img/main/newsImg1.png" alt=""/></div>
				<strong>글로벌 핀테크 강자 페이팔 금융혁신 글로벌 핀테크 강자 페이팔 금융혁신</strong>
				<span>전자신문 <em></em> 1분전</span>
			</a>
			<a href="#">
				<div class="newsImg"><img src="/chatbit_m/img/main/newsImg2.png" alt=""/></div>
				<strong>블록체인 가상화폐 기술 교류 다빈치 글 블록체인 가상화폐 기술 교류 다빈치 글</strong>
				<span>매일경제 <em></em> 8분전</span>
			</a>
		</div>
		<ul class="boardList">
			<li>
				<a href="#">
					<strong>실리콘밸리의 핀테크와 제도권 금융을 결합 실리콘밸리의 핀테크와 제도권 금융을 결합</strong>
					<span>토큰포스트 <em></em> 1일전</span>
				</a>
			</li>
			<li>
				<a href="#">
					<strong>영국 스마트 컨트랙트 산업을 위한 법 제정 영국 스마트 컨트랙트 산업을 위한 법 제정</strong>
					<span>전자신문 <em></em> 1일전</span>
				</a>
			</li>
			<li>
				<a href="#">
					<strong>기관 '순매수' 코스피 5일반에 반등 '개소 기관 '순매수' 코스피 5일반에 반등 '개소...</strong>
					<span>전자신문 <em></em> 2일전</span>
				</a>
			</li>
		</ul>
	</div>
	<hr class="h9"/>

	<div class="banner">
		<div class="swiper-wrapper">
			<div class="swiper-slide">
				<a href="#"><img src="/chatbit_m/img/main/mainBanner.png" alt=""/></a>
			</div>
			<div class="swiper-slide">
				<a href="#"><img src="/chatbit_m/img/main/mainBanner.png" alt=""/></a>
			</div>
		</div>
		<div class="swiper-pagination"></div>
	</div>

	<div class="main_section">
		<p class="tit boardTabTit">
			<strong>코인톡톡</strong>
			<a href="/chatbit_m/toktok.jsp"></a>
		</p>
		<div class="boardTab">
			<a href="javascript:;" name="/chatbit_m/toktok.jsp" class="on">코인톡톡</a>
			<a href="javascript:;" name="/chatbit_m/toktok.jsp">투자일기</a>
			<a href="javascript:;" name="/chatbit_m/toktok.jsp">분석게시판</a>
			<a href="javascript:;" name="/chatbit_m/toktok.jsp">자유게시판</a>
		</div>
		<div class="boardList2">
			<ul class="boardCont1">
				<li>
					<a href="#">
						<strong class="tit">올스타빗 스케치 오늘만 거의 2배 됐네요.</strong>
						<span class="txt">이달 초 기사와 같이 서클은 고객에게 더 많은 서비스를 제공할 수 있도록 미국에서 연방 은행 라이센스를 얻을 예정이며 증권</span>
						<span class="bot">
							<span class="name">코인뉴스</span>
							<span class="good">45</span>
							<span class="reple">1234</span>
						</span>
					</a>
				</li>
				<li>
					<a href="#">
						<strong class="tit">올스타빗 스케치 오늘만 거의 2배 됐네요.</strong>
						<span class="txt">비트소닉 지금 가입하면 거래가능한 코인50개 줍니다. (BSC) = 비트소닉 코인 장외거래방 오픈톡에서 개당 90원에 판매되고 있습니다.</span>
						<span class="bot">
							<span class="name">자산관리자</span>
							<span class="good">45</span>
							<span class="reple">1234</span>
						</span>
					</a>
				</li>
				<li>
					<a href="#">
						<strong class="tit">디지털 광고 시장의 문제점과 루시디티의 문제점을 알아보자.</strong>
						<span class="txt">글로벌 핀테크 강자 페이팔 금융혁신 진두지휘, ‘주목할 만한 혁신가’ 15인 가상뱅킹: 혁신과 파트너링 가이드</span>
						<span class="bot">
							<span class="name">블록미디어</span>
							<span class="good">45</span>
							<span class="reple">1234</span>
						</span>
					</a>
				</li>
			</ul>
			<ul class="boardCont2">
				<li>
					<a href="#">
						<strong class="tit">22올스타빗 스케치 오늘만 거의 2배 됐네요.</strong>
						<span class="txt">이달 초 기사와 같이 서클은 고객에게 더 많은 서비스를 제공할 수 있도록 미국에서 연방 은행 라이센스를 얻을 예정이며 증권</span>
						<span class="bot">
							<span class="name">코인뉴스</span>
							<span class="good">45</span>
							<span class="reple">1234</span>
						</span>
					</a>
				</li>
				<li>
					<a href="#">
						<strong class="tit">올스타빗 스케치 오늘만 거의 2배 됐네요.</strong>
						<span class="txt">비트소닉 지금 가입하면 거래가능한 코인50개 줍니다. (BSC) = 비트소닉 코인 장외거래방 오픈톡에서 개당 90원에 판매되고 있습니다.</span>
						<span class="bot">
							<span class="name">자산관리자</span>
							<span class="good">45</span>
							<span class="reple">1234</span>
						</span>
					</a>
				</li>
				<li>
					<a href="#">
						<strong class="tit">디지털 광고 시장의 문제점과 루시디티의 문제점을 알아보자.</strong>
						<span class="txt">글로벌 핀테크 강자 페이팔 금융혁신 진두지휘, ‘주목할 만한 혁신가’ 15인 가상뱅킹: 혁신과 파트너링 가이드</span>
						<span class="bot">
							<span class="name">블록미디어</span>
							<span class="good">45</span>
							<span class="reple">1234</span>
						</span>
					</a>
				</li>
			</ul>
			<ul class="boardCont3">
				<li>
					<a href="#">
						<strong class="tit">33올스타빗 스케치 오늘만 거의 2배 됐네요.</strong>
						<span class="txt">이달 초 기사와 같이 서클은 고객에게 더 많은 서비스를 제공할 수 있도록 미국에서 연방 은행 라이센스를 얻을 예정이며 증권</span>
						<span class="bot">
							<span class="name">코인뉴스</span>
							<span class="good">45</span>
							<span class="reple">1234</span>
						</span>
					</a>
				</li>
				<li>
					<a href="#">
						<strong class="tit">올스타빗 스케치 오늘만 거의 2배 됐네요.</strong>
						<span class="txt">비트소닉 지금 가입하면 거래가능한 코인50개 줍니다. (BSC) = 비트소닉 코인 장외거래방 오픈톡에서 개당 90원에 판매되고 있습니다.</span>
						<span class="bot">
							<span class="name">자산관리자</span>
							<span class="good">45</span>
							<span class="reple">1234</span>
						</span>
					</a>
				</li>
				<li>
					<a href="#">
						<strong class="tit">디지털 광고 시장의 문제점과 루시디티의 문제점을 알아보자.</strong>
						<span class="txt">글로벌 핀테크 강자 페이팔 금융혁신 진두지휘, ‘주목할 만한 혁신가’ 15인 가상뱅킹: 혁신과 파트너링 가이드</span>
						<span class="bot">
							<span class="name">블록미디어</span>
							<span class="good">45</span>
							<span class="reple">1234</span>
						</span>
					</a>
				</li>
			</ul>
			<ul class="boardCont4">
				<li>
					<a href="#">
						<strong class="tit">44올스타빗 스케치 오늘만 거의 2배 됐네요.</strong>
						<span class="txt">이달 초 기사와 같이 서클은 고객에게 더 많은 서비스를 제공할 수 있도록 미국에서 연방 은행 라이센스를 얻을 예정이며 증권</span>
						<span class="bot">
							<span class="name">코인뉴스</span>
							<span class="good">45</span>
							<span class="reple">1234</span>
						</span>
					</a>
				</li>
				<li>
					<a href="#">
						<strong class="tit">올스타빗 스케치 오늘만 거의 2배 됐네요.</strong>
						<span class="txt">비트소닉 지금 가입하면 거래가능한 코인50개 줍니다. (BSC) = 비트소닉 코인 장외거래방 오픈톡에서 개당 90원에 판매되고 있습니다.</span>
						<span class="bot">
							<span class="name">자산관리자</span>
							<span class="good">45</span>
							<span class="reple">1234</span>
						</span>
					</a>
				</li>
				<li>
					<a href="#">
						<strong class="tit">디지털 광고 시장의 문제점과 루시디티의 문제점을 알아보자.</strong>
						<span class="txt">글로벌 핀테크 강자 페이팔 금융혁신 진두지휘, ‘주목할 만한 혁신가’ 15인 가상뱅킹: 혁신과 파트너링 가이드</span>
						<span class="bot">
							<span class="name">블록미디어</span>
							<span class="good">45</span>
							<span class="reple">1234</span>
						</span>
					</a>
				</li>
			</ul>
		</div>
	</div>
	<hr class="h9"/>

	<div class="main_section">
		<p class="tit">
			<strong>코인프로</strong>
			<a href="#"></a>
		</p>
		<div class="coinPro">
			<div class="swiper-coinPro">
				<div class="swiper-wrapper">
					<a href="#" class="swiper-slide coinproCon">
						<img src="/chatbit_m/img/main/coinproImg.png" alt=""/>
						<span class="photo"><img src="/chatbit_m/img/main/photoImg.png" alt=""/></span>
						<strong class="tit">CBOE, SEC에 비트코인 ETF 신청  </strong>
						<span class="txt">올해 6월 26일 SEC 공식 웹사이트 에 게시된 문서에 따르면 CB케OE 이라고 하는데</span>
						<span class="bot">
							<span class="name">코인뉴스</span>
							<span class="good">45</span>
							<span class="reple">1234</span>
						</span>
					</a>
					<a href="#" class="swiper-slide coinproCon">
						<img src="/chatbit_m/img/main/coinproImg.png" alt=""/>
						<span class="photo"><img src="/chatbit_m/img/main/photoImg.png" alt=""/></span>
						<strong class="tit">CBOE, SEC에 비트코인 ETF 신청  </strong>
						<span class="txt">올해 6월 26일 SEC 공식 웹사이트 에 게시된 문서에 따르면 CB케OE Gl…</span>
						<span class="bot">
							<span class="name">코인뉴스</span>
							<span class="good">45</span>
							<span class="reple">1234</span>
						</span>
					</a>
					<a href="#" class="swiper-slide coinproCon">
						<img src="/chatbit_m/img/main/coinproImg.png" alt=""/>
						<span class="photo"><img src="/chatbit_m/img/main/photoImg.png" alt=""/></span>
						<strong class="tit">CBOE, SEC에 비트코인 ETF 신청  </strong>
						<span class="txt">올해 6월 26일 SEC 공식 웹사이트 에 게시된 문서에 따르면 CB케OE Gl…</span>
						<span class="bot">
							<span class="name">코인뉴스</span>
							<span class="good">45</span>
							<span class="reple">1234</span>
						</span>
					</a>
					<a href="#" class="swiper-slide coinproCon">
						<img src="/chatbit_m/img/main/coinproImg.png" alt=""/>
						<span class="photo"><img src="/chatbit_m/img/main/photoImg.png" alt=""/></span>
						<strong class="tit">CBOE, SEC에 비트코인 ETF 신청  </strong>
						<span class="txt">올해 6월 26일 SEC 공식 웹사이트 에 게시된 문서에 따르면 CB케OE Gl…</span>
						<span class="bot">
							<span class="name">코인뉴스</span>
							<span class="good">45</span>
							<span class="reple">1234</span>
						</span>
					</a>
				</div>
			</div>
		</div>
	</div>
	<hr class="h9"/>

	<div class="main_section">
		<p class="tit">
			<strong>수익인증</strong>
			<a href="#"></a>
		</p>
		<div class="coinCertify">
			<div class="coin-certify">
				<div class="swiper-wrapper">
					<a href="#" class="swiper-slide coinCertyCon">
						<span class="img">
                            <img src="/chatbit_m/img/main/certyImg1.png" class="certyImg" alt="" />
                            <span class="certyIco">
                                <img src="/chatbit_m/img/svg/arr_v.svg" width="8" alt="" />
						</span>
                        </span>
						<strong class="tit">백만원으로 비트코인 구매하면 배당금</strong>
					</a>
					<a href="#" class="swiper-slide coinCertyCon">
						<span class="img">
                            <img src="/chatbit_m/img/main/certyImg2.png" class="certyImg" alt="" />
                            <span class="certyIco">
                                <img src="/chatbit_m/img/svg/arr_v.svg" width="8" alt="" />
						</span>
						</span>
						<strong class="tit">Coinex 배당수익</strong>
					</a>
					<a href="#" class="swiper-slide coinCertyCon">
						<span class="img">
                            <img src="/chatbit_m/img/main/certyImg1.png" class="certyImg" alt="" />
                            <span class="certyIco minus">
                                <img src="/chatbit_m/img/svg/arr_v.svg" width="8" alt="" />
						</span>
						</span>
						<strong class="tit">7월 존버</strong>
					</a>
					<a href="#" class="swiper-slide coinCertyCon">
						<span class="img">
                            <img src="/chatbit_m/img/main/certyImg2.png" class="certyImg" alt="" />
                            <span class="certyIco">
                                <img src="/chatbit_m/img/svg/arr_v.svg" width="8" alt="" />
						</span>
						</span>
						<strong class="tit">완전 손실 봤네요! 다 날리게 되쓰요</strong>
					</a>
				</div>
			</div>
		</div>
	</div>
	<hr class="h9"/>

	<div class="main_section">
		<p class="tit">
			<strong>인기채팅</strong>
			<a href="#"></a>
		</p>
		<ul class="chatList">
			<li>
				<a href="#">
					<img src="http://img.phinf.pholar.net/20161207_293/1481113352656YszKU_JPEG/p" alt=""/>
					<strong class="tit">8월 터질 코인들 대공개! </strong>
					<span class="txt rank1">자산관리자</span>
					<span class="num">53</span>
				</a>
			</li>
			<li>
				<a href="#">
					<img src="http://img.phinf.pholar.net/20161207_293/1481113352656YszKU_JPEG/p" alt=""/>
					<strong class="tit">수익을 챙기는 코인거래법</strong>
					<span class="txt rank2">자산관리자</span>
					<span class="num">53</span>
				</a>
			</li>
			<li>
				<a href="#">
					<img src="http://img.phinf.pholar.net/20161207_293/1481113352656YszKU_JPEG/p" alt=""/>
					<strong class="tit">비트코인 930만원/+216954092183467453</strong>
					<span class="txt rank3">자산관리자</span>
					<span class="num">53</span>
				</a>
			</li>
			<li>
				<a href="#">
					<img src="http://img.phinf.pholar.net/20161207_293/1481113352656YszKU_JPEG/p" alt=""/>
					<strong class="tit">수익을 챙기는 코인거래법</strong>
					<span class="txt">자산관리자</span>
					<span class="num">53</span>
				</a>
			</li>
			<li>
				<a href="#">
					<img src="http://img.phinf.pholar.net/20161207_293/1481113352656YszKU_JPEG/p" alt=""/>
					<strong class="tit">코인거래에 필요한 용어 정리 및 기초 상식</strong>
					<span class="txt">자산관리자</span>
					<span class="num">53</span>
				</a>
			</li>
		</ul>
	</div>
	<hr class="h18"/>

	<p class="floating">
        <a href="#" class="jsLiveChat">
            <span class="btnLiveChat">
                <span class="visuallyhidden">Contact me in real time</span>
            </span>
        </a>
        <a href="javascript:;" class="topIco">
            <span class="btnToTop">
                <span class="visuallyhidden">Go to top</span>
            </span>
        </a>
	</p>

	<script src="/chatbit_m/js/swiper.js"></script>
	<script>
		var swiper = new Swiper('.swiper-container', {
			slidesPerView:7
		});
		var swiper2 = new Swiper('.swiper-coinPro', {
			slidesPerView:'auto'
		});
		var swiper3 = new Swiper('.coin-certify', {
			slidesPerView:'auto'
		});
		var swiper4 = new Swiper('.banner', {
			slidesPerView:1,
			pagination:{
				el:'.swiper-pagination',
				type:'bullets',
			}
		});

		$('.coinPriceTab li').each(function(){
			$(this).find('a').on('click',function(){
				$(this).parent().addClass('on').siblings().removeClass('on');
				$('.coinPriceCont>div').hide();
				$('.cpc_'+$(this).parent().attr('data-tab')).show();
				return false;
			})
		})

		$('.coinPriceBt').on('click',function(){
			$(this).toggleClass('on');
			$('.coinPriceWrap').slideToggle();
			return false;
		})

		$('.coinSettingBt').on('click',function(){
			$('.coinSetting').fadeIn();
			scrollHeight = $(window).scrollTop();
			$('body').css({
				'position': 'fixed',
				'top': '-' + scrollHeight + 'px'
			})
			return false;
		})

		$('.coinSettingClose').on('click',function(){
			$('.coinSetting').fadeOut();
			$('body').css({
				'position': 'static'
			})
			$('html, body').animate({
				scrollTop: scrollHeight + 'px'
			}, 10);
			return false;
		})

		$('.boardTab a').each(function(){
			$(this).on('click',function(){
				$(this).addClass('on').siblings().removeClass('on');
				$('.boardList2>ul').hide();
				$('.boardList2 .boardCont'+($(this).index()+1)).show();

				$('.boardTabTit>strong').text($(this).text());
				$('.boardTabTit>a').attr('href',$(this).attr('name'));

				return false;
			})
		})

		$('.topIco').on('click',function(){
			$('html, body').animate({
				scrollTop:0
			},500)
        })
        
		function floating(){
			if ($(document).scrollTop() > $(document).height() - $(window).height() - 150){
				$('.floating').css('bottom',$(document).scrollTop() - ($(document).height() - $(window).height() - 160));
			}else{
				$('.floating').css('bottom','10px');
			}
		}
		floating();

		function topIco(){
			if ($(document).scrollTop() > 100){
				$('.topIco').show();
			}else{
				$('.topIco').hide();
			}
		}
		topIco();
		$(window).scroll(function(){
			topIco();
			floating();
		})
	</script>

</div>

<jsp:include page="/chatbit_m/include/chatApp.jsp" flush="true"></jsp:include>
<script>
    $('.jsLiveChat').click(function(){
        var $chatLayer = $('.jsChatApp');
        $chatLayer.fadeIn().find('.chat-container').scrollTop( $chatLayer.find('.chat-container-inner').outerHeight());
    });
</script>

<jsp:include page="/chatbit_m/include/footer.jsp" flush="true"></jsp:include>

</body>
</html>
