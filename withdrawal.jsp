<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" %>
<html lang="ko">

<head>
    <title> CHATBIT - YMAX </title>
    <meta charset="utf-8" />
    <meta name="format-detection" content="telephone=no" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <link href="/chatbit_m/css/base.css" type="text/css" rel="stylesheet" />
    <link href="/chatbit_m/css/layout.css" type="text/css" rel="stylesheet" />
    <link href="/chatbit_m/css/common.css" type="text/css" rel="stylesheet" />
    <link href="/chatbit_m/css/member.css" type="text/css" rel="stylesheet" />
    <script src="/chatbit/js/1.10.1.jquery.min.js" type="text/javascript" charset="utf-8"></script>
</head>

<body>

    <jsp:include page="/chatbit_m/include/header.jsp" flush="true"></jsp:include>

    <div class="containerWrap pageWithdrawal">

        <h2 class="pageTitle">회원탈퇴</h2>

        <h3 class="withdrawalTitle">
            챗비트 회원탈퇴를 신청하시기 전에<br /> 안내사항을 확인해 주세요
        </h3>
        <ul class="cListDash withdrawalRuleList">
            <li>사용하고 계신
                <strong class="colorFb">facebook</strong> 계정은 탈퇴하실 경우
                <span class="colorPrimary">재가입 또는 복구가 불가능</span>합니다.</li>
            <li>탈퇴 후 회원정보는 모두 삭제되며 삭제된 데이터는 복구되지 않습니다.
                <div class="definitionList">
                    <dl>
                        <dt>포인트</dt>
                        <dd>포인트 소멸 및 약관동의 철회</dd>
                    </dl>
                    <dl>
                        <dt>팔로우</dt>
                        <dd>모든 팔로우 목록 삭제</dd>
                    </dl>
                    <dl>
                        <dt>쪽지</dt>
                        <dd>저장된 쪽지 삭제</dd>
                    </dl>
                    <dl>
                        <dt>기타</dt>
                        <dd>계정에 연계된 개인적 정보 삭제</dd>
                    </dl>
                </div>
            </li>
            <li>탈퇴 후에도 게시판 서비스에 등록된 게시물과 댓글, 추천은 삭제되지 않습니다. </li>
            <li>삭제를 원하시는 글이 있다면 반드시
                <span class="colorSecondary">탈퇴 전에 삭제하시기 바랍니다.</span>
            </li>
            <li>탈퇴 후에는 본인 여부를 확인할 수 없기 때문에 게시글을 삭제해드릴 수 없습니다.</li>
            <li>탈퇴 후에는 현재 등록된 SNS 계정으로 다시 가입할 수 없으며 회원정보와 데이터는 복구할 수 없습니다. </li>
            <li>게시판에 남아있는 게시글은 탈퇴 후 삭제할 수 없습니다. </li>
        </ul>
        <p class="withdrawalAgree">
            <span class="cCheckbox">
                <input type="checkbox" id="agreeAll" class="visuallyhidden">
                <label for="agreeAll" class="cCheckboxLabel">안내사항을 모두 확인했으며 이에 동의합니다.</label>
            </span>
        </p>
        <div class="withdrawalButton">
            <button class="cBtn">확인</button>
        </div>

        <a href="./memberOut.jsp">회원탈퇴 완료 페이지 이동</a>
    </div>

    <jsp:include page="/chatbit_m/include/footer.jsp" flush="true"></jsp:include>

</body>

</html>