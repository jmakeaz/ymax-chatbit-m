<%@ page contentType="text/html; charset=utf-8" %>

<!-- <link href="/chatbit/summernote/bootstrap.css" type="text/css" rel="stylesheet" />
<link href="/chatbit/summernote/summernote.css" type="text/css" rel="stylesheet" />
<script src="/chatbit/summernote/bootstrap.js" type="text/javascript" charset="utf-8"></script>
<script src="/chatbit/summernote/summernote.js" type="text/javascript" charset="utf-8"></script> -->

<div class="headerWrap">
    <h1 class="logo"><a href="#"><img src="/chatbit_m/img/svg/bi.svg" alt="" width="103" /></a></h1>
    <a href="javascript:;" class="menuBt"><img src="/chatbit_m/img/svg/sidebar.svg" alt="" width="20" /></a>
    <span class="headR">
        <a href="javascript:;" class="searchBt"><img src="/chatbit_m/img/svg/search.svg" alt="" height="18" /></a>
        <a href="#" class="writeBt"><img src="/chatbit_m/img/svg/write.svg" alt="" height="18" /></a>
    </span>
</div>
<div class="searchArea">
    <form method="post" action="">
        <input type="text" class="it" title="" value="" name="" placeholder="검색어를 입력하세요." />
    </form>
    <a href="javascript:;" class="searchAreaClose"></a>
</div>

<div class="mob_gnb">
    <a href="javascript:;" class="mobGnbCloseBt"><img src="/chatbit_m/img/svg/cl_s.svg" alt="" width="13" /></a>
    <div class="userArea">
        <a href="#" class="loginBefore">
            <span class="img"></span> 로그인을 해주세요.
        </a>
        <div class="loginAfter">
            <a href="#" class="userPhoto">
                <span class="img"><img src="http://img.phinf.pholar.net/20161207_293/1481113352656YszKU_JPEG/p" alt="" /></span>
                <strong>자산관리사</strong>
                <span>Level 57</span>
            </a>
            <div class="userAlram">
                <a href="#" class="alram">알림 <span>2</span></a>
                <a href="#" class="memo">쪽지 <span>1</span></a>
                <a href="#" class="logout">로그아웃</a>
            </div>
            <div class="changeArea">
                <p class="CP">
                    <strong>CBP</strong>
                    <span>19,234</span>
                </p>
                <p class="CP">
                    <strong>CGP</strong>
                    <span>1,234,567,890</span>
                </p>
                <a href="#" class="changeBt">
                    <span>Exchange</span>
                </a>
            </div>
        </div>
    </div>
    <div class="gnb">
        <a href="/chatbit_m/" class="home">챗비트 홈</a>
        <ul class="">
            <li><a href="#" class="game">모의투자</a></li>
            <li><a href="#">코인뉴스</a></li>
            <li class="sub"><a href="#">톡톡게시판</a>
                <ul class="">
                    <li><a href="#">코인톡톡</a></li>
                    <li><a href="#">수익인증</a></li>
                    <li><a href="#">투자일기</a></li>
                    <li><a href="#">분석게시판</a></li>
                    <li><a href="#">자유게시판</a></li>
                </ul>
            </li>
            <li><a href="#">코인프로</a></li>
            <li><a href="#">채팅</a></li>
            <li class="sub"><a href="#">고객센터</a>
                <ul class="">
                    <li><a href="#">공지사항</a></li>
                    <li><a href="#">문의하기</a></li>
                    <li><a href="#">운영정책</a></li>
                </ul>
            </li>
        </ul>
    </div>
</div>
<!-- <hr class="h9" /> -->

<div class="cPopup popupWritePost">
    <div class="cPopupInner">
        <h2 class="cPopupTitle">글쓰기</h2>
        <button type="button" class="cPopupClose jsPopupClose" aria-label="Close this popup">
            <span class="btnX"></span>
        </button>

        <!-- D: 수익인증 글쓰기일 때는 typeCerti 클래스명 추가 -->
        <div class="cPopupBody typeCerti">
            <div class="selectBoardType">
                <select>
                    <option value="1" selected>코인톡톡</option>
                    <option value="2">수익인증</option>
                    <option value="3">투자일기</option>
                    <option value="4">분석게시판</option>
                    <option value="5">자유게시판</option>
                    <option value="6">코인프로</option>
                </select>
            </div>

            <p class="inputField writeTitle">
                <label for="tokTitle" class="visuallyhidden">제목</label>
                <span class="input">
                    <input type="text" id="tokTitle" value="" placeholder="제목을 입력해 주세요.">
                </span>
                <strong class="message">사용할 수 없습니다.</strong>
            </p>

            <div class="writeCategory">
                <label class="cRadio2 plus">
                    <input type="radio" class="visuallyhidden" name="category" value="plus" checked="">
                    <span class="cRadio2-text">
                        수익
                    </span>
                </label>
                <label class="cRadio2">
                    <input type="radio" class="visuallyhidden" name="category" value="minus">
                    <span class="cRadio2-text">
                        손실
                    </span>
                </label>
            </div>

            <div class="writeBodyWrap jsEditor">
                <div class="writeBody">
                    <textarea class="editor"></textarea>
                </div>
                <div class="editorPlaceholder">
                    <ul>
                        <li>글 작성 시 15포인트가 적립됩니다.</li>
                        <li>작성하신 글이 추천을 받거나 댓글이 달릴 때마다 추가 포인트가 적립됩니다.</li>
                        <li>일정 댓글/추천수를 넘을 경우 게시글의 등급이 변경되며 변경 시 추가 포인트가 적립됩니다.</li>
                        <li>추가 포인트에 대한 자세한 안내는 공지사항을 참조해 주세요.</li>
                    </ul>
                </div>
            </div>

            <div class="writeBodyMeta">
                <span class="imgUpload cBtn2">
                    <button type="button" class="figment">
                        <span class="iconPhoto"></span>이미지 첨부
                    </button>
                    <input type="file" accept="image/*" class="uploadInputFile">
                </span>

                <span class="writeEditorLength">0/4000</span>
            </div>

            <div class="writeButtons rowGroup">
                <button type="button" class="cBtn line">취소</button>
                <button class="cBtn jsConfirm">등록</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    /* layer bg - dim */
    function dimOn() {
        $('body').prepend('<div class="dim"></div>');
        $('.dim').fadeIn('slow');
    }

    //메뉴 열기 & 닫기
    var scrollHeight = '0';
    $('.menuBt').on('click', function () {
        scrollHeight = $(window).scrollTop();
        $('body').css({
            'position': 'fixed',
            'top': '-' + scrollHeight + 'px'
        })
        $('.mob_gnb').animate({
            'left': '0'
        }, 300);
        dimOn();
    });

    function mGnbPage() {
        $('body').css({
            'position': 'static'
        })
        $('.mob_gnb').animate({
            'left': '-256px'
        }, 300, function () {
            $('.gnb .sub').find('>a').removeClass('active').next().slideDown();
        });
        $('html, body').animate({
            scrollTop: scrollHeight + 'px'
        }, 10);
        $('.dim').fadeOut('slow', function () {
            $('.dim').remove();
        });
    }
    $('.mobGnbCloseBt').on('click', function () {
        mGnbPage();
    });


    //sub 메뉴 토글
    $('.gnb .sub').find('>a').on('click', function () {
        $(this).toggleClass('active').next().slideToggle();
        return false;
    })

    $('body').on('click', '.jsPopupClose', function () {
        $(this).parents('.cPopup').fadeOut(200);
    });

    $('.writeBt').click(function () {
        $('.popupWritePost').fadeIn(200);
        // $('html').css('overflow', 'hidden');
    });

    $('.searchBt').on('click', function () {
        $('.searchArea').fadeIn();
        scrollHeight = $(window).scrollTop();
        $('body').css({
            'position': 'fixed',
            'top': '-' + scrollHeight + 'px'
        })
        return false;
    })

    $('.searchAreaClose').on('click', function () {
        $('.searchArea').fadeOut();
        $('body').css({
            'position': 'static'
        })
        $('html, body').animate({
            scrollTop: scrollHeight + 'px'
        }, 10);
        return false;
    })

    $(document).ready(function () {

        // 글쓰기 플레이스홀더
        $('.jsEditor textarea').each(function (i, element) {
            var $editor = $(element);
            var $placeholder = $editor.parents('.jsEditor').find('.editorPlaceholder');

            if ($editor.val() !== "") {
                $placeholder.hide();
            }
            $editor.focus(function () {
                $placeholder.hide();
            })
            $editor.blur(function () {
                if ($editor.val() == "") {
                    $placeholder.show();
                }
            });
        });
    });
</script>