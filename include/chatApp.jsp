<%@ page contentType="text/html; charset=utf-8" %>

<link href="/chatbit_m/chatApp/index.css" type="text/css" rel="stylesheet" />

<!-- D: 비공개일 경우 privateRoom 클래스명 추가 -->
<!-- D: 글 작성 중일 경우(textarea에 focus) writing 클래스명 추가 -->
<!-- D: header 없는 경우 mini 클래스명 추가 -->
<!-- <div class="chat-wrapper privateRoom mini jsChatApp"> -->
<div class="chat-wrapper privateRoom jsChatApp">
    <button type="button" class="chat-header-exit jsChatClose">
        <span class="btnX"></span>
        <span class="visuallyhidden">Close</span>
    </button>
    <div class="chat-header">
        <p class="chat-header-name">
            abcdefghijklmnopqrstuvwxyz 수익을 챙기는 코인거래법!
        </p>
        <!-- <strong class="chat-header-label">공개</strong> -->
        <strong class="chat-header-label">비공개</strong>
        <span class="chat-header-number">
            <span class="iconPeople"></span>
            13
        </span>
        <time class="chat-header-time">
            <span class="iconTime"></span>
            00:02:54
        </time>
    </div>
    <div class="chat-container">
        <div class="chat-container-inner">
            <div class="chat-msg me">
                <div class="chat-msg-item">
                    <div class="chat-msg-item-text">안녕하세요~</div>
                    <time class="chat-msg-item-time">PM 04:05</time>
                </div>
            </div>

            <div class="chat-msg me">
                <div class="chat-msg-item">
                    <div class="chat-msg-item-text">반갑습니다.</div>
                    <time class="chat-msg-item-time">PM 04:05</time>
                </div>
            </div>

            <div class="chat-msg">
                <span class="chat-msg-user">
                    <a href="#" class="chat-msg-user-avatar">
                        <img src="/chatbit_m/chatApp/img/@temp-user.png" alt="">
                    </a>
                    <span class="chat-msg-user-name">자산관리자</span>
                </span>
                <div class="chat-msg-item">
                    <div class="chat-msg-item-text">안녕하세요! 반갑습니다.</div>
                    <time class="chat-msg-item-time">PM 04:05</time>
                </div>
            </div>

            <div class="chat-msg">
                <div class="chat-msg-item">
                    <div class="chat-msg-item-text">네이버가 카페 회원 간 채팅 내용을 저장하고
                        이를 열람할 수 있게 했다. 인터넷 서비스 운영
                        업체가 사용자의 대화 내용을 들여다볼 수
                        <br> 있어 사회적 논란이 예상된다</div>
                    <time class="chat-msg-item-time">PM 04:05</time>
                </div>
            </div>

            <div class="chat-msg">
                <span class="chat-msg-user">
                    <a href="#" class="chat-msg-user-avatar">
                        <img src="" alt="">
                    </a>
                    <span class="chat-msg-user-name">자산관리자</span>
                </span>
                <div class="chat-msg-item">
                    <div class="chat-msg-item-text">뿅!!</div>
                    <time class="chat-msg-item-time">PM 04:05</time>
                </div>
            </div>

            <div class="chat-msg me">
                <div class="chat-msg-item">
                    <div class="chat-msg-item-text">뿡뿡이 천재</div>
                    <time class="chat-msg-item-time">PM 04:05</time>
                </div>
            </div>

            <div class="chat-msg me">
                <div class="chat-msg-item">
                    <div class="chat-msg-item-text">뿡뿡이 천재</div>
                    <time class="chat-msg-item-time">PM 04:05</time>
                </div>
            </div>

            <div class="chat-msg me">
                <div class="chat-msg-item">
                    <div class="chat-msg-item-text">뿡뿡이 천재</div>
                    <time class="chat-msg-item-time">PM 04:05</time>
                </div>
            </div>
        </div>
        <!-- // .chat-container-inner -->
    </div>
    <div class="chat-footer">
        <textarea class="chat-footer-editor" placeholder="메세지를 입력해주세요." id="chatEditor"></textarea>
        <button type="button" class="chat-footer-btn">
            <span class="iconSend"></span>
            <span class="visuallyhidden">보내기</span>
        </button>
    </div>
</div>

<script>
    function scrollMoveBottom(gap) {
        var $parent = $('.chat-container');
        $parent.scrollTop($parent.scrollTop() + Number(gap));
    }

    $(document).ready(function () {

        // 쪽지 textarea 높이 
        var $chatEditor = $('#chatEditor');
        $chatEditor.focus(function () {
            $chatEditor.parents('.chat-wrapper').addClass('writing');
            scrollMoveBottom(+38);
        });
        $chatEditor.blur(function () {
            scrollMoveBottom(-38);
            $chatEditor.parents('.chat-wrapper').removeClass('writing');
        });
    });

    $('.jsChatClose').click(function () {
        $('.jsChatApp').fadeOut(200);
    });
</script>