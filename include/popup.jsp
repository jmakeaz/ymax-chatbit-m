<%@ page contentType="text/html; charset=utf-8" %>

<!-- 이용 정지 안내 팝업 -->
<div class="bubbleNotice jsNoticePopup">
    <h2 class="bubbleNoticeTitle">Chatbit 이용 정지 안내</h2>
    <div class="bubbleNoticeContent">
        <p>
            회원님은 관리자 심의 결과 <strong>Chatbit</strong> 운영 정책을
            위반하여 커뮤니티 이용 정지 처분을 받게
            되었음을 알려드립니다.
        </p>
        <dl class="bubbleNoticeDetail">
            <dt>이용 정지 기간:</dt>
            <dd>2018.07.23 15:32 ~ 2018.07.29 15:32</dd>
            <dt>사유:</dt>
            <dd>
                개인정보 이용 제한<br />
                (타인의 개인정보 도용 및 유포)
            </dd>
        </dl>
        <p>
            보다 구체적인 탈퇴 사유를 알고 싶으시다면
            <a href="#">운영정책</a>과 <a href="#">이용약관</a>을 참고해 주세요.
        </p>
    </div>
    <button type="button" class="cBtn">확인</button>
</div>

<!-- 회원신고 팝업 -->
<div class="cPopup popupReport">
    <div class="cPopupInner">
        <h2 class="cPopupTitle">회원신고</h2>
        <button type="button" class="cPopupClose jsPopupClose" aria-label="Close this popup">
            <span class="btnX"></span>
        </button>

        <!-- D: 수익인증 글쓰기일 때는 typeCerti 클래스명 추가 -->
        <div class="cPopupBody">
            <form action="#" method="post">
                <p class="text1">이 회원님을 신고하는 이유가 무엇인가요?</p>
                <p class="text2">신고 접수 시 <strong>내용증명을 위한 캡쳐 이미지나
                        url 등 근거를 제시</strong>하셔야 하며 관리자는 해당
                    회원에게 사실 관계를 확인 후 징계 등 제재
                    조치를 취하게 됩니다.</p>
                <div class="writeBodyWrap jsEditor">
                    <div class="writeBody">
                        <textarea class="editor"></textarea>
                    </div>
                    <div class="editorPlaceholder">
                        신고사유를 입력해 주세요.<br />
                        단 근거없는 비방 등으로 신고 접수 시
                        관리자가 대응하지 않을 수 있습니다.
                        조치에 대해 이의가 있으신 경우
                        <span class="colorPrimary">고객센터 > 문의하기 게시판</span>을 통해 접수할
                        수 있습니다.
                    </div>
                </div>

                <div class="writeBodyMeta">
                    <span class="imgUpload cBtn2">
                        <button type="button" class="figment">
                            <span class="iconPhoto"></span>이미지 첨부
                        </button>
                        <input type="file" accept="image/*" class="uploadInputFile">
                    </span>

                    <span class="writeEditorLength">0/4000</span>
                </div>

                <div class="writeButtons">
                    <button class="cBtn jsConfirm">신고하기</button>
                </div>
            </form>
        </div>
        <!-- // .cPopupBody -->
    </div>
</div>

<!-- CGP 교환 팝업 -->
<div class="cPopup popupExchange jsPopupExchange">
    <div class="cPopupInner">
        <h2 class="cPopupTitle">CGP 교환</h2>
        <button type="button" class="cPopupClose jsPopupClose" aria-label="Close this popup">
            <span class="btnX"></span>
        </button>

        <!-- D: 수익인증 글쓰기일 때는 typeCerti 클래스명 추가 -->
        <div class="cPopupBody popupExchangeContent">
            <div class="cgp-exchange-tabs" role="tablist">
                <button type="button" class="cgp-exchange-tab active" role="tab">CBP
                    <span class="to"></span> CGP
                </button>
                <button type="button" class="cgp-exchange-tab" role="tab">CBP
                    <span class="to"></span> CGP
                </button>
            </div>

            <!-- CBP → CGP -->
            <div class="tab-contents" role="tabpanel">
                <p class="exchange-title-img">
                    <img src="/chatbit_m/img/toCgp.png" alt="CBP를 CGP로 교환" width="250" />
                </p>
                <div class="cgp-exchange-info-wrap">
                    <dl class="cgp-exchange-info">
                        <dt>보유 CBP</dt>
                        <dd>1,234</dd>
                    </dl>
                    <dl class="cgp-exchange-info">
                        <dt>교환가능 CBP</dt>
                        <dd>1,234</dd>
                    </dl>
                </div>
                <form class="cgp-exchange-form">
                    <fieldset>
                        <div class="cgp-exchange-input">
                            <p class="inputField">
                                <label for="fromCBP" class="label">교환할 CBP</label>
                                <span class="input blueLine">
                                    <input type="number" id="fromCBP" value="" placeholder="교환할 CBP를 입력하세요.">
                                    <span class="unit">CBP</span>
                                </span>
                                <strong class="message">Validation text</strong>
                            </p>
                        </div>
                        <div class="cgp-exchange-input">
                            <p class="inputField">
                                <label for="toCGP" class="visuallyhidden">교환될 CGP</label>
                                <span class="input">
                                    <input type="number" id="toCGP" value="0" readonly="">
                                    <span class="unit">CGP</span>
                                </span>
                                <strong class="message">Validation text</strong>
                            </p>
                        </div>
                        <p class="exchange-unit-info">* 1CBP = 100CGP </p>
                    </fieldset>
                </form>
            </div>
            <!-- // CBP → CGP -->

            <!-- CGP → CBP -->
            <div class="tab-contents toCbp" role="tabpanel" style="display: none;">
                <p class="exchange-title-img">
                    <img src="/chatbit_m/img/toCbp.png" alt="CGP를 CGP로 교환" width="250" />
                </p>
                <div class="cgp-exchange-info-wrap">
                    <dl class="cgp-exchange-info">
                        <dt>보유 CGP</dt>
                        <dd>1,224,7214</dd>
                    </dl>
                    <dl class="cgp-exchange-info">
                        <dt>교환가능 CGP</dt>
                        <dd>1,234</dd>
                    </dl>
                </div>
                <form class="cgp-exchange-form">
                    <fieldset>
                        <div class="cgp-exchange-input">
                            <p class="inputField">
                                <label for="fromCGP" class="label">교환할 CGP</label>
                                <span class="input">
                                    <input type="number" id="fromCGP" value="" placeholder="교환할 CGP를 입력하세요.">
                                    <span class="unit">CGP</span>
                                </span>
                                <strong class="message">Helper text</strong>
                            </p>
                        </div>
                        <div class="cgp-exchange-input">
                            <p class="inputField">
                                <label for="toCBP" class="visuallyhidden">교환될 CBP</label>
                                <span class="input">
                                    <input type="number" id="toCBP" value="0" readonly="">
                                    <span class="unit">CBP</span>
                                </span>
                                <strong class="message">Helper text</strong>
                            </p>
                        </div>
                        <p class="exchange-unit-info">* 1CBP = 100CGP </p>
                    </fieldset>
                </form>
            </div>
            <!-- // CGP → CBP -->

            <button type="button" class="cBtn popupExchangeBtn">교환하기</button>
        </div>
        <!-- // .cPopupBody -->
    </div>
</div>

<script>
    $('.cgp-exchange-tabs button').each(function () {
        var $this = $(this);
        $this.on('click', function () {
            var tabIndex = $this.index();
            $this
                .addClass('active')
                .siblings()
                .removeClass('active');
            $('.popupExchangeContent')
                .find('.tab-contents')
                .eq(tabIndex)
                .show()
                .siblings('.tab-contents')
                .hide();
        })
    })
</script>