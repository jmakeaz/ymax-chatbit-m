<%@ page contentType="text/html; charset=utf-8" %>

<div class="boardSelect">
    <button class="boardLabel jsBoardMenu">코인톡톡</button>
    <div class="boardLinks jsBoardLinks">
        <!-- <ul>
            <li data-value="1" class="active">
                <a href="#">코인톡톡</a>
            </li>
            <li data-value="2">
                <a href="#">수익인증</a>
            </li>
            <li data-value="3">
                <a href="#">투자일기</a>
            </li>
            <li data-value="4">
                <a href="#">분석게시판</a>
            </li>
            <li data-value="5">
                <a href="#">자유게시판</a>
            </li>
            <li data-value="6">
                <a href="#">코인프로</a>
            </li>
        </ul> -->
        <ul id="jsBoardLinks">
            <li data-value="2"><a href="#">코인뉴스</a></li>
            <li data-value="4" class="active"><a href="#">코인톡톡</a></li>
            <li data-value="5"><a href="#">수익 인증</a></li>
            <li data-value="16"><a href="#">ICO&amp;포럼</a></li>
            <li data-value="3"><a href="#">분석게시판</a></li>
            <li data-value="6"><a href="#">투자일기</a></li>
            <li data-value="1"><a href="#">자유게시판</a></li>
            <li data-value="17"><a href="#">홍보톡톡</a></li>
            <li data-value="9"><a href="#">코인프로(필진)</a></li>
            <li data-value="18"><a href="#">동영상강좌</a></li>
            <li data-value="13"><a href="#">이벤트</a></li>
            <li data-value="14"><a href="#">참여인증</a></li>
            <li data-value="15"><a href="#">당첨자발표</a></li>
        </ul>
    </div>
</div>

<script>
    function closeBoardDim(menu) {
        var $menuButton = $(menu);
        var $dim = $('.dim2');

        $menuButton.removeClass('active');
        $dim.fadeOut('slow', function () {
            $dim.remove();
        });
    }

    // 게시판 페이지에서 셀렉트박스 메뉴 선택
    $('.jsBoardMenu').click(function () {
        var $this = $(this);
        var $body = $('body');
        var $toTop = $body.find('.floating');

        if (!$this.hasClass('active')) {
            $('.containerWrap').prepend('<div class="dim2 jsBoardDim"></div>').find('.dim2').fadeIn('slow');
            $this.addClass('active');
            if ($body.has('.floating').length) {
                $toTop.css({
                    zIndex: 1
                });
            }
        } else {
            closeBoardDim(this);
            $toTop.css({
                zIndex: 99
            });
        }
    });

    // 게시판 메뉴 딤 클릭시 메뉴 닫기
    $('body').on('touchstart click', '.jsBoardDim', function () {
        closeBoardDim('.jsBoardMenu');
    });

    // 게시판 메뉴 선택
    $('.jsBoardLinks').on('click', 'li', function () {
        var $this = $(this);
        $('.jsBoardLinks').find('.active').removeClass('active');
        $this.addClass('active');
        $('.jsBoardMenu').text($this.text());
        closeBoardDim('.jsBoardMenu');
    });
</script>