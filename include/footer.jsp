<%@ page contentType="text/html; charset=utf-8" %>

<jsp:include page="/chatbit_m/include/popup.jsp" flush="true"></jsp:include>

<div class="footer">
    <p class="footMenu">
        <a href="#">회사소개</a>
        <a href="#">이용약관</a>
        <a href="#" class="point">개인정보처리방침</a>
        <a href="#">회원탈퇴</a>
    </p>
    <div class="change">
        <p class="langSelectArea">
            <label for="langSelect">한국어</label>
            <select name="" title="" id="langSelect">
                <option value="" selected="selected">한국어</option>
                <option value="">영어</option>
            </select>
        </p>
        <a href="#" class="goToPc">PC버전</a>
    </div>
    <address>
        ㈜YMAX 고객센터 :
        <a href="mailTo:help@ymax.co.kr">help@ymax.co.kr</a> ㆍ
        <a href="tel:02-863-3370">02-863-3370</a>
        <br />Copyright©YMAX Corp. All rights reserved.
    </address>
</div>

<script>
    /* layer bg - dim */
    function dimOn() {
        $('body').prepend('<div class="dim"></div>').find('.dim').fadeIn('slow');
        // $('.dim').fadeIn('slow');
    }

    /* layer close (dim remove) */
    function dimOff() {
        $('.dim').fadeOut('slow', function () {
            $('.dim').remove();
        });
    }

    // 기사글 제목 말줄임표
    $(function () {
        if ($('body').has('.ellipsis_multiple').length) {
            $('.ellipsis_multiple').each(function (i, element) {
                $(element).dotdotdot();
            });
        }
    });
</script>