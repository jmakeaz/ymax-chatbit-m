<%@ page contentType="text/html; charset=utf-8" %>

<div class="boardSelect">
    <button class="boardLabel jsBoardMenu">공지사항</button>
    <div class="boardLinks jsBoardLinks">
        <ul>
            <li data-value="1" class="active">
                <a href="#">공지사항</a>
            </li>
            <li data-value="2">
                <a href="#">문의하기</a>
            </li>
            <li data-value="3">
                <a href="#">운영정책</a>
            </li>
        </ul>
    </div>
</div>

<script>
    function closeBoardDim(menu) {
        var $menuButton = $(menu);
        var $dim = $('.dim2');

        $menuButton.removeClass('active');
        $dim.fadeOut('slow', function () {
            $dim.remove();
        });
    }

    // 게시판 페이지에서 셀렉트박스 메뉴 선택
    $('.jsBoardMenu').click(function () {
        var $this = $(this);
        var $body = $('body');

        if (!$this.hasClass('active')) {
            $('.containerWrap').prepend('<div class="dim2 jsBoardDim"></div>').find('.dim2').fadeIn('slow');
            $this.addClass('active');
        } else {
            closeBoardDim(this);
        }
    });

    // 게시판 메뉴 딤 클릭시 메뉴 닫기
    $('body').on('touchstart click', '.jsBoardDim', function () {
        closeBoardDim('.jsBoardMenu');
    });

    // 게시판 메뉴 선택
    $('.jsBoardLinks').on('click', 'li', function () {
        var $this = $(this);
        $('.jsBoardLinks').find('.active').removeClass('active');
        $this.addClass('active');
        $('.jsBoardMenu').text($this.text());
        closeBoardDim('.jsBoardMenu');
    });
</script>