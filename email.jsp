<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" %>
<html lang="ko">

<head>
    <title> CHATBIT - YMAX </title>
    <meta charset="utf-8" />
    <meta name="format-detection" content="telephone=no" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <link href="/chatbit_m/css/base.css" type="text/css" rel="stylesheet" />
    <link href="/chatbit_m/css/layout.css" type="text/css" rel="stylesheet" />
    <link href="/chatbit_m/css/common.css" type="text/css" rel="stylesheet" />
    <link href="/chatbit_m/css/member.css" type="text/css" rel="stylesheet" />
    <script src="/chatbit/js/1.10.1.jquery.min.js" type="text/javascript" charset="utf-8"></script>
</head>

<body>

    <div class="containerWrap">
        <div class="headerWrap">
            <h1 class="logo"><a href="#"><img src="/chatbit_m/img/svg/bi.svg" alt="" width="103"></a></h1>
        </div>

        <div class="emailConfirmPage">
            <h2 class="title">이메일 인증</h2>
            <p class="descBold">이메일 인증이 필요합니다.</p>
            <p class="desc">
                계정찾기 등 고객지원 서비스를 위해 이메일 인증을
                받고 있습니다. 전송된 이메일을 확인하신 후 인증
                링크를 클릭해 주세요. 인증이 완료되면
                Chatbit의 서비스를 이용하실 수 있습니다.
            </p>

            <form action="#">
                <p class="inputField">
                    <label for="email" class="label">인증메일 발송 주소</label>
                    <span class="input">
                        <input type="email" id="email" value="chatbit@gmail.com" placeholder="이메일을 입력해주세요." readonly>
                    </span>
                    <strong class="message">사용할 수 없습니다.</strong>
                </p>
                <button class="cBtn line">인증메일 재전송</button>
            </form>

            <p class="notice">
                메일에 따라 도착 시간이 조금 지연될 수 있습니다.
                오랫동안 오지 않을 경우 스팸메일함을 확인해 주세요.
            </p>

            <a href="/chatbit_m/" class="cBtn goMain">메인으로 이동</a>
        </div>
    </div>

    <a href="./email2.jsp">이메일 인증 완료 페이지</a>

    <jsp:include page="/chatbit_m/include/footer.jsp" flush="true"></jsp:include>

</body>

</html>