<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" %>
<html lang="ko">

<head>
    <title> CHATBIT - YMAX </title>
    <meta charset="utf-8" />
    <meta name="format-detection" content="telephone=no" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <link href="/chatbit_m/css/base.css" type="text/css" rel="stylesheet" />
    <link href="/chatbit_m/css/layout.css" type="text/css" rel="stylesheet" />
    <link href="/chatbit_m/css/common.css" type="text/css" rel="stylesheet" />
    <link href="/chatbit_m/css/mypage.css" type="text/css" rel="stylesheet" />
    <script src="/chatbit/js/1.10.1.jquery.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="/chatbit_m/js/jquery.dotdotdot.js" type="text/javascript" charset="utf-8"></script>
</head>

<body>

    <jsp:include page="/chatbit_m/include/header.jsp" flush="true"></jsp:include>

    <div class="containerWrap pageBookmark">

        <h2 class="pageTitle">북마크</h2>

        <!-- article list -->
        <div class="articleList">
            <p class="noData">북마크 된 데이터가 없습니다.</p>
            <div class="article haveImg">
                <a href="/chatbit_m/toktokView.jsp">
                    <div class="articleImage">
                        <img src="https://placekitten.com/640/360" alt="">
                    </div>
                    <h3 class="articleTitle ellipsis_label">
                        [코인뉴스] 세계 최대 암호 화폐 비트메인, 새정책 발표 정보 공개 사업 투
                        <span class="labelWrap">
                            <span class="newsLabel">대표글</span>
                        </span>
                    </h3>
                    <p class="articleInfo">
                        <span class="source">관리자</span>
                        <time class="time">2018.09.28</time>
                        <span class="cComments">99+</span>
                    </p>
                </a>
                <button type="button" class="btnBookmark active">
                    <span class="iconBookmark"></span>
                    <span class="visuallyhidden">북마크</span>
                </button>
            </div>
            <div class="article">
                <a href="/chatbit_m/toktokView.jsp">
                    <div class="articleImage">
                        <!-- <img src="https://www.fillmurray.com/640/360" alt=""> -->
                    </div>
                    <h3 class="articleTitle ellipsis_label">
                        [자유게시판] 세계 최대 암호화폐 채굴기업 책 발표..."정보 공개로 사업 투명성
                        <span class="labelWrap">
                            <span class="newsLabel">HOT</span>
                        </span>
                    </h3>
                    <p class="articleInfo">
                        <span class="source">돌아온김상사</span>
                        <time class="time">2018.09.28</time>
                        <span class="cComments">99+</span>
                    </p>
                </a>
                <button type="button" class="btnBookmark active">
                    <span class="iconBookmark"></span>
                    <span class="visuallyhidden">북마크</span>
                </button>
            </div>
            <div class="article">
                <a href="/chatbit_m/toktokView.jsp">
                    <div class="articleImage">
                        <!-- <img src="https://placekitten.com/640/360" alt=""> -->
                    </div>
                    <h3 class="articleTitle ellipsis_label">
                        [코인톡톡] 실리콘밸리의 핀테크와 제도권 금융을 결합시킨 혁신가, 댄 샷
                        <span class="labelWrap">
                            <span class="newsLabel">추천</span>
                        </span>
                    </h3>
                    <p class="articleInfo">
                        <span class="source">돌아온김상사</span>
                        <time class="time">2018.09.28</time>
                        <span class="cComments">45</span>
                    </p>
                </a>
                <button type="button" class="btnBookmark active">
                    <span class="iconBookmark"></span>
                    <span class="visuallyhidden">북마크</span>
                </button>
            </div>
            <div class="article">
                <a href="/chatbit_m/toktokView.jsp">
                    <div class="articleImage">
                        <!-- <img src="https://placekitten.com/640/360" alt=""> -->
                    </div>
                    <h3 class="articleTitle ellipsis_label">
                        [코인뉴스] 바이낸스 자오 창펑 “탈중앙거래소가 미래”
                    </h3>
                    <p class="articleInfo">
                        <span class="source">코인부자</span>
                        <time class="time">2018.09.28</time>
                        <span class="cComments">1</span>
                    </p>
                </a>
                <button type="button" class="btnBookmark active">
                    <span class="iconBookmark"></span>
                    <span class="visuallyhidden">북마크</span>
                </button>
            </div>
            <div class="article">
                <a href="/chatbit_m/toktokView.jsp">
                    <div class="articleImage">
                        <!-- <img src="https://placekitten.com/640/360" alt=""> -->
                    </div>
                    <h3 class="articleTitle ellipsis_label">
                        [코인뉴스] 아이콘루프, 카카오·SK플래닛 출신 플랫폼 전문가 영입
                    </h3>
                    <p class="articleInfo">
                        <span class="source">코인부자</span>
                        <time class="time">2018.09.28</time>
                        <span class="cComments">1</span>
                    </p>
                </a>
                <button type="button" class="btnBookmark active">
                    <span class="iconBookmark"></span>
                    <span class="visuallyhidden">북마크</span>
                </button>
            </div>
        </div>
        <!-- article list -->
        <button type="button" class="articleMore">더보기</button>

    </div>

    <hr class="h9" />

    <jsp:include page="/chatbit_m/include/footer.jsp" flush="true"></jsp:include>
    <script>
        // 기사글 제목 말줄임표
        $(function () {
            $('.ellipsis_label').each(function (i, element) {
                $(element).dotdotdot({
                    keep: '.labelWrap'
                });
            });
        });
    </script>

</body>

</html>