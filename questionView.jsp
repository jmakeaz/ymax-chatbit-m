<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" %>
<html lang="ko">

<head>
    <title> CHATBIT - YMAX </title>
    <meta charset="utf-8" />
    <meta name="format-detection" content="telephone=no" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <link href="/chatbit_m/css/base.css" type="text/css" rel="stylesheet" />
    <link href="/chatbit_m/css/layout.css" type="text/css" rel="stylesheet" />
    <link href="/chatbit_m/css/common.css" type="text/css" rel="stylesheet" />
    <link href="/chatbit_m/css/board.css" type="text/css" rel="stylesheet" />
    <script src="/chatbit/js/1.10.1.jquery.min.js" type="text/javascript" charset="utf-8"></script>
</head>

<body>

    <jsp:include page="/chatbit_m/include/header.jsp" flush="true"></jsp:include>

    <div class="containerWrap">

        <jsp:include page="/chatbit_m/include/customerMenu.jsp" flush="true"></jsp:include>
        <script>
            $('.jsBoardLinks [data-value="2"]').trigger('click');
        </script>

        <div class="articleViewHeader typeSimple">
            <h2 class="title">문의드립니다.</h2>
            <p class="info">
                <span class="time">2018.08.09 11:34</span>
            </p>
            <!-- // .info -->

            <div class="info2">
                <div class="articleExtraFnWrap">
                    <button type="button" class="btnExtra jsExtraFn active">
                        <span class="visuallyhidden">더보기</span>
                    </button>
                    <div class="articleExtraFn">
                        <ul class="extraFnList">
                            <li class="fontResize">
                                가
                                <button type="button" class="btnZoomout">
                                    <span class="visuallyhidden">글씨축소</span>
                                </button>
                                <button type="button" class="btnZoomin">
                                    <span class="visuallyhidden">글씨확대</span>
                                </button>
                            </li>
                            <!-- <li>
                                <button type="button" class="extraFnItem jsShare">
                                    <span class="iconShare"></span>
                                    공유
                                </button>
                            </li>
                            <li>
                                <button type="button" class="extraFnItem jsBookmark">
                                    <span class="iconBookmark"></span>
                                    북마크
                                </button>
                            </li>
                            <li>
                                <button type="button" class="extraFnItem">
                                    <span class="iconReport"></span>
                                    신고
                                </button>
                            </li> -->
                        </ul>
                    </div>
                </div>
            </div>
            <!-- // .info2 -->
        </div>
        <!-- // .articleViewHeader -->

        <div class="articleViewContents">
            <div class="qnaMyQ">
                <strong class="visuallyhidden">Question</strong>
                포인트 변환을 하다가 에러가 났는데 제 포인트는 줄었는데 게임 포인트는 추가가 안됐거든요…….. 제 포인트……………
                확인 좀 부탁드려요.
            </div>
            <div class="qnaMyA">
                <strong class="visuallyhidden">Answer</strong>
                죄송합니다. 2018년 8월 9일 15시 30분에 전환 신청하신 포인트가 CGP로 전환되지 않음이 확인되어 바로 시정하였습니다.
                현재 고객님의 CGP는 12,345,678이며
                불편을 끼친 것에 대한 사죄로 1,000 CGP를 추가로 보내드렸습니다.
                Chatbit에서 즐거운 시간 보내시기 바랍니다. 감사합니다.
            </div>
        </div>
        <!-- // .articleViewContents -->

        <div class="articleViewButtons">
            <a href="./notice.jsp" class="cBtn2">목록</a>
        </div>

        <!-- 페이징 -->
        <div class="viewPagingWrap">
            <a href="#" class="viewPaging disabled">
                <p>
                    <span class="direction">이전글</span>
                    <span class="title ellipsis">이전글이 없습니다.</span>
                </p>
            </a>
            <a href="./noticeView.jsp" class="viewPaging">
                <p>
                    <span class="direction">다음글</span>
                    <span class="title ellipsis">추석 연휴 고객센터 휴무 안내</span>
                </p>
            </a>
        </div>
        <!-- 페이징 -->
    </div>

    <jsp:include page="/chatbit_m/include/footer.jsp" flush="true"></jsp:include>

</body>

</html>