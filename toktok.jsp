<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" %>
<html lang="ko">

<head>
    <title> CHATBIT - YMAX </title>
    <meta charset="utf-8" />
    <meta name="format-detection" content="telephone=no" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <link href="/chatbit_m/css/base.css" type="text/css" rel="stylesheet" />
    <link href="/chatbit_m/css/layout.css" type="text/css" rel="stylesheet" />
    <link href="/chatbit_m/css/common.css" type="text/css" rel="stylesheet" />
    <link href="/chatbit_m/css/board.css" type="text/css" rel="stylesheet" />
    <script src="/chatbit/js/1.10.1.jquery.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="/chatbit_m/js/jquery.dotdotdot.js" type="text/javascript" charset="utf-8"></script>
</head>

<body>

    <jsp:include page="/chatbit_m/include/header.jsp" flush="true"></jsp:include>

    <div class="containerWrap">

        <jsp:include page="/chatbit_m/include/boardMenu.jsp" flush="true"></jsp:include>
        <script>
            $('.jsBoardLinks [data-value="1"]').trigger('click');
        </script>

        <!-- 게시글이 없을 경우 -->
        <p class="noData">게시글이 없습니다.</p>

        <!-- sort -->
        <div class="sortingGroup">
            <label class="orderCheckbox jsChatOrder">
                <!-- input이 checked 되면 인기순 -->
                <input type="checkbox" class="visuallyhidden" value="order">
                <span class="orderCheckbox-text">최신순</span>
            </label>
        </div>
        <!-- // sort -->

        <!-- article list -->
        <div class="articleList">
            <div class="article">
                <a href="/chatbit_m/toktokView.jsp">
                    <div class="articleImage">
                        <!-- <img src="https://placekitten.com/640/360" alt=""> -->
                    </div>
                    <h3 class="articleTitle ellipsis_label">
                        [공지] Chatbit 포인트 정책 Ver 1.5이 새로 업데이트 되었습니다.
                        <span class="labelWrap">
                            <span class="labelNew" aria-label="New">N</span>
                        </span>
                    </h3>
                    <p class="articleInfo">
                        <span class="source">관리자</span>
                        <time class="time">2018.09.28</time>
                        <span class="cComments">99+</span>
                    </p>
                </a>
                <!-- <div class="aritcleExtra">
                    <a href="#" class="edit">수정</a>
                    <button class="delete jsMyPostDelete">삭제</button>
                </div> -->
            </div>
            <div class="article haveImg">
                <a href="/chatbit_m/toktokView.jsp">
                    <div class="articleImage">
                        <img src="https://www.fillmurray.com/640/360" alt="">
                    </div>
                    <h3 class="articleTitle ellipsis_label">
                        세계 최대 암호화폐 채굴기 업 비트메인, 새정책 발표 정보 공개 사업 투명...
                        <span class="labelWrap">
                            <span class="newsLabel">대표글</span>
                            <span class="labelNew" aria-label="New">N</span>
                        </span>
                    </h3>
                    <p class="articleInfo">
                        <span class="source">돌아온김상사</span>
                        <time class="time">2018.09.28</time>
                        <span class="cComments">99+</span>
                    </p>
                </a>
            </div>
            <div class="article">
                <a href="/chatbit_m/toktokView.jsp">
                    <div class="articleImage">
                        <!-- <img src="https://placekitten.com/640/360" alt=""> -->
                    </div>
                    <h3 class="articleTitle ellipsis_label">
                        세계 최대 암호화폐 채굴기업 비트메인, 새 정책 발표..."정보 공개로 사업 투명성...
                        <span class="labelWrap"><span class="newsLabel">HOT</span></span>
                    </h3>
                    <p class="articleInfo">
                        <span class="source">돌아온김상사</span>
                        <time class="time">2018.09.28</time>
                        <span class="cComments">45</span>
                    </p>
                </a>
            </div>
            <div class="article">
                <a href="/chatbit_m/toktokView.jsp">
                    <div class="articleImage">
                        <!-- <img src="https://placekitten.com/640/360" alt=""> -->
                    </div>
                    <h3 class="articleTitle ellipsis_label">
                        실리콘밸리의 핀테크와 제도권 금융을 결합시킨 혁신가, 댄 샷
                        <span class="labelWrap"><span class="newsLabel">추천</span></span>
                    </h3>
                    <p class="articleInfo">
                        <span class="source">코인부자</span>
                        <time class="time">2018.09.28</time>
                        <span class="cComments">1</span>
                    </p>
                </a>
            </div>
            <div class="article blocked">
                <a href="/chatbit_m/toktokView.jsp">
                    <div class="articleImage">
                        <!-- <img src="https://placekitten.com/640/360" alt=""> -->
                    </div>
                    <h3 class="articleTitle ellipsis_label">
                        바이낸스 자오 창펑 “탈중앙 거래소가 미래”
                    </h3>
                    <p class="articleInfo">
                        <span class="source">코인부자</span>
                        <time class="time">2018.09.28</time>
                        <span class="cComments">1</span>
                    </p>
                </a>
            </div>
            <div class="article">
                <a href="/chatbit_m/toktokView.jsp">
                    <div class="articleImage">
                        <!-- <img src="https://placekitten.com/640/360" alt=""> -->
                    </div>
                    <h3 class="articleTitle ellipsis_label">
                        아이콘루프, 카카오·SK플래닛 출신 플랫폼 전문가 영입
                    </h3>
                    <p class="articleInfo">
                        <span class="source">코인부자</span>
                        <time class="time">2018.09.28</time>
                        <span class="cComments">1</span>
                    </p>
                </a>
            </div>
        </div>
        <!-- article list -->
        <button type="button" class="articleMore">더보기</button>
    </div>

    <hr class="h9" />

    <jsp:include page="/chatbit_m/include/footer.jsp" flush="true"></jsp:include>
    <script>
        // 체크박스가 선택되면 인기순으로 변경
        $('.jsChatOrder').click(function () {
            var $this = $(this);
            var $input = $this.find('input');
            var $text = $this.find('.orderCheckbox-text');

            if ($input.prop('checked')) {
                $text.text('인기순');
            } else {
                $text.text('최신순');
            }
        });

        // 기사글 제목 말줄임표
        $(function () {
            $('.ellipsis_label').each(function (i, element) {
                $(element).dotdotdot({
                    keep: '.labelWrap'
                });
            });
        });
    </script>

</body>

</html>