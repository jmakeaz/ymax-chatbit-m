<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" %>
<html lang="ko">

<head>
    <title> CHATBIT - YMAX </title>
    <meta charset="utf-8" />
    <meta name="format-detection" content="telephone=no" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <link href="/chatbit_m/css/base.css" type="text/css" rel="stylesheet" />
    <link href="/chatbit_m/css/layout.css" type="text/css" rel="stylesheet" />
    <link href="/chatbit_m/css/common.css" type="text/css" rel="stylesheet" />
    <link href="/chatbit_m/css/mypage.css" type="text/css" rel="stylesheet" />
    <script src="/chatbit/js/1.10.1.jquery.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="/chatbit_m/js/jquery.dotdotdot.js" type="text/javascript" charset="utf-8"></script>
</head>

<body>

    <jsp:include page="/chatbit_m/include/header.jsp" flush="true"></jsp:include>

    <div class="containerWrap">

        <h2 class="pageTitle">프로필</h2>

        <div class="myInfo">
            <div class="myInfoVariety">
                <div class="myInfoAvatar">
                    <span class="cProfileImg size60">
                        <img src="https://placekitten.com/620/320?image=3" alt="">
                    </span>
                </div>
                <p class="myInfoName">
                    워렌 버핏
                    <span class="myInfoLevel">Lebel 57</span>
                </p>

                <!-- D: 내 프로필일 경우 보여주기 -->
                <button type="button" class="btnSetting jsProfileSetting">
                    <span class="iconSetting"></span>
                    <span class="visuallyhidden">Edit My Profile</span>
                </button>

                <!-- D: 다른 사람 프로필일 경우 보여주기 -->
                <button type="button" class="btnReport jsMemberReport">
                    <span class="iconReport"></span>
                    <span class="visuallyhidden">Report this post</span>
                </button>

                <div class="myInfoAction">
                    <!-- D: 팔로잉 할 경우 following 클래스 추가 -->
                    <button class="btnToggleFollow cBtn2 small fill">팔로우</button>

                    <!-- 나의 프로필에서 쪽지 버튼 클릭시 페이지 이동
                                                다른 사람 프로필에서 쪽지 버튼 클릭시 쪽지 입력창 -->
                    <a href="#" class="btnLetter jsLetterBtn">
                        <span class="visuallyhidden">Go to the Message page Or Send Message</span>
                        <span class="iconLetter"></span>
                        <span class="btnLetterLength">1</span>
                    </a>
                </div>
            </div>
            <div class="myInfoDesc">
                가상화폐 전문가.신규코인 정보 나눔.정보공유환영. 광고사절.부자기원.대박기원
                궁금한 점이 있으시면 언제든지 쪽지 보내주세요!
            </div>
            <div class="myInfoMeta">
                <span class="item">게시글
                    <span class="value">123</span>
                </span>
                <button type="button" class="item jsFollowerList">팔로워
                    <span class="value">456</span>
                </button>
                <button type="button" class="item jsFollowList">팔로우
                    <span class="value">789</span>
                </button>
            </div>
        </div>
        <!-- // .myInfo -->

        <div class="cTabs myPageTabs" role="tablist">
            <a href="#myPost" class="cTabItem post" role="tab" aria-selected="true">
                <span class="iconPost"></span>
                <span class="visuallyhidden">게시글</span>
            </a>
            <a href="#myNotice" class="cTabItem notice" role="tab" id="btn_vw_alarm" aria-selected="false">
                <span class="iconNotice"></span>
                <span class="visuallyhidden">알림</span>
            </a>
            <a href="#myPoint" class="cTabItem point" role="tab" id="btn_vw_point" aria-selected="false">
                <span class="iconPoint"></span>
                <span class="visuallyhidden">포인트</span>
            </a>
        </div>

        <div class="tabContent">
            <!-- 게시글 -->
            <div id="myPost" class="mypost" role="tabpanel" style="display: block;">
                <h3 class="visuallyhidden">게시글</h3>

                <!-- article list -->
                <div class="articleList">
                    <div class="article">
                        <a href="/chatbit_m/toktokView.jsp">
                            <div class="articleImage">
                                <!-- <img src="https://placekitten.com/640/360" alt=""> -->
                            </div>
                            <h3 class="articleTitle ellipsis_label">
                                [코인톡톡] 이더리움 기반 블록체인 프로젝트 체인질리 거래소 상장 엔진스마트...
                                <span class="labelWrap"><span class="newsLabel">HOT</span></span>
                            </h3>
                            <time class="articleInfoTime">2018.09.28</time>
                        </a>
                        <p class="articleInfo">
                            <a href="#" class="edit">수정</a>
                            <button class="delete jsMyPostDelete">삭제</button>
                            <span class="cComments">45</span>
                            <span class="cLikes">1234</span>
                        </p>
                    </div>
                    <div class="article haveImg">
                        <a href="/chatbit_m/toktokView.jsp">
                            <div class="articleImage">
                                <img src="https://www.fillmurray.com/640/360" alt="">
                            </div>
                            <h3 class="articleTitle ellipsis_label">
                                [투자일기] 실리콘밸리의 핀테크와 제도권 금융을
                                <span class="labelWrap"><span class="newsLabel">대표글</span></span>
                            </h3>
                            <time class="articleInfoTime">2018.09.28</time>
                        </a>
                        <p class="articleInfo">
                            <a href="#" class="edit">수정</a>
                            <button class="delete jsMyPostDelete">삭제</button>
                            <span class="cComments">45</span>
                            <span class="cLikes">1234</span>
                        </p>
                    </div>
                    <div class="article haveImg plus">
                        <a href="/chatbit_m/toktokView.jsp">
                            <div class="articleImage">
                                <img src="/chatbit_m/img/none2.png" alt="">
                            </div>
                            <h3 class="articleTitle ellipsis_label">
                                [수익인증] 지금 나만 수익 낼 수 있는거냐 ??
                            </h3>
                            <time class="articleInfoTime">2018.09.28</time>
                        </a>
                        <p class="articleInfo">
                            <a href="#" class="edit">수정</a>
                            <button class="delete jsMyPostDelete">삭제</button>
                            <span class="cComments">45</span>
                            <span class="cLikes">1234</span>
                        </p>
                    </div>
                    <div class="article haveImg minus">
                        <a href="/chatbit_m/toktokView.jsp">
                            <div class="articleImage">
                                <img src="https://www.fillmurray.com/640/360" alt="">
                            </div>
                            <h3 class="articleTitle ellipsis_label">
                                [수익인증] 지금 나만 수익 낼 수 있는거냐 ??
                            </h3>
                            <time class="articleInfoTime">2018.09.28</time>
                        </a>
                        <p class="articleInfo">
                            <a href="#" class="edit">수정</a>
                            <button class="delete jsMyPostDelete">삭제</button>
                            <span class="cComments">45</span>
                            <span class="cLikes">1234</span>
                        </p>
                    </div>
                    <div class="article">
                        <a href="/chatbit_m/toktokView.jsp">
                            <div class="articleImage">
                                <!-- <img src="https://placekitten.com/640/360" alt=""> -->
                            </div>
                            <h3 class="articleTitle ellipsis_label">
                                [분석게시판] 스위스 재정적 인 규칙은 envion AG에 대하여 시행 절차를 발사합니다.
                            </h3>
                            <time class="articleInfoTime">2018.09.28</time>
                        </a>
                        <p class="articleInfo">
                            <a href="#" class="edit">수정</a>
                            <button class="delete jsMyPostDelete">삭제</button>
                            <span class="cComments">45</span>
                            <span class="cLikes">1234</span>
                        </p>
                    </div>
                    <div class="article">
                        <a href="/chatbit_m/toktokView.jsp">
                            <div class="articleImage">
                                <!-- <img src="https://placekitten.com/640/360" alt=""> -->
                            </div>
                            <h3 class="articleTitle ellipsis_label">
                                [자유게시판] 이더리움 기반 블록체인 프로젝트 체인질리 거래소 상장 엔진스마트월렛
                            </h3>
                            <time class="articleInfoTime">2018.09.28</time>
                        </a>
                        <p class="articleInfo">
                            <a href="#" class="edit">수정</a>
                            <button class="delete jsMyPostDelete">삭제</button>
                            <span class="cComments">45</span>
                            <span class="cLikes">1234</span>
                        </p>
                    </div>
                </div>
                <!-- article list -->
                <button type="button" class="articleMore">더보기</button>
            </div>

            <!-- 알림 -->
            <div id="myNotice" role="tabpanel" style="display: none;">
                <h3 class="visuallyhidden">알림</h3>
                <ul class="myNoticeList">
                    <li>
                        <a href="#" class="noticeItem disabled">
                            <span class="cProfileImg noticeItemImg">
                                <img src="/chatbit_m/img/@temp-man.png" alt="">
                            </span>
                            <p class="noticeItemText">
                                홍길동님이 새로운 글을 작성하였습니다.
                                <em>‘비트코인 USD 차트 분석'</em>
                            </p>
                            <span class="noticeItemTime">2018.7.23 17:33</span>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="noticeItem">
                            <span class="cProfileImg noticeItemImg">
                                <img src="/chatbit_m/img/@temp-man.png" alt="">
                            </span>
                            <p class="noticeItemText">
                                김영희님이 회원님의 글을 추천합니다.
                                <em>‘빗썸 350억원 해킹’</em>
                            </p>
                            <span class="noticeItemTime">2018.7.23 17:33</span>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="noticeItem">
                            <span class="cProfileImg noticeItemImg">
                                <img src="/chatbit_m/img/@temp-man.png" alt="">
                            </span>
                            <p class="noticeItemText">
                                김철수님이 회원님의 글에 댓글을 남겼습니다.
                                <em>‘좋은 정보 감사합니다’</em>
                            </p>
                            <span class="noticeItemTime">2018.7.23 17:33</span>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="noticeItem">
                            <span class="cProfileImg noticeItemImg">
                                <!-- <img src="https://placekitten.com/640/360" alt=""> -->
                            </span>
                            <p class="noticeItemText">
                                구동매님이 쪽지를 보냈습니다.
                            </p>
                            <span class="noticeItemTime">2018.7.23 17:33</span>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="noticeItem">
                            <span class="cProfileImg noticeItemImg">
                                <!-- <img src="https://placekitten.com/640/360" alt=""> -->
                            </span>
                            <p class="noticeItemText">
                                김희성님이 회원님을 팔로우합니다.
                            </p>
                            <span class="noticeItemTime">2018.7.23 17:33</span>
                        </a>
                    </li>

                    <!-- D: 공지사항일 경우에 adminNotice 클래스 토글 -->
                    <li class="adminNotice">
                        <a href="#" class="noticeItem">
                            <span class="cProfileImg noticeItemImg">
                                <!-- <img src="/chatbit_m/img/@temp-man.png" alt=""> -->
                            </span>
                            <p class="noticeItemText">
                                8월 1일 01:00~01:20까지 서버점검을 실시합니다. 자세한 사항은 공지사항을 참조해
                                주세요. 읽어주셔서 감사합니다.
                                주세요. 읽어주셔서 감사합니다.
                                주세요. 읽어주셔서 감사합니다.
                                주세요. 읽어주셔서 감사합니다.
                            </p>
                            <span class="noticeItemTime">2018.7.23 17:33</span>
                        </a>
                    </li>
                </ul>
                <button type="button" class="articleMore">더보기</button>
            </div>

            <!-- 포인트 -->
            <div id="myPoint" role="tabpanel" style="display: none;">

                <div class="myPointInfo">
                    <h3 class="myPointTitle">나의 포인트(CBP)</h3>
                    <div class="myPointInfoTable">
                        <p class="myPointInfoItem">
                            <strong class="title">적립포인트</strong>
                            <span class="value">1,234P</span>
                        </p>
                        <p class="myPointInfoItem">
                            <strong class="title">사용포인트</strong>
                            <span class="value">56,789P</span>
                        </p>
                    </div>
                    <button type="button" class="cBtn2 icon btnExchange jsExchange">
                        <span class="iconExchange"></span>CGP 교환
                    </button>
                </div>
                <!-- myPointInfo -->

                <ul class="myPointHistory">
                    <li>
                        <div class="pointItem">
                            <span class="pointItemState">
                                적립
                            </span>
                            <p class="pointItemText">
                                <strong class="mainText ellipsis">글쓰기 적립 글쓰기 적립 글쓰기 적립</strong>
                                <span class="time">2018.7.23 17:33</span>
                            </p>
                            <strong class="pointItemValue">
                                15<span>P</span>
                            </strong>
                        </div>
                    </li>
                    <li>
                        <div class="pointItem">
                            <span class="pointItemState">
                                적립
                            </span>
                            <p class="pointItemText">
                                <strong class="mainText">첫번째 추천글</strong>
                                <span class="time">2018.7.23 17:33</span>
                            </p>
                            <strong class="pointItemValue">
                                100<span>P</span>
                            </strong>
                        </div>
                    </li>
                    <li>
                        <div class="pointItem">
                            <span class="pointItemState">
                                적립
                            </span>
                            <p class="pointItemText">
                                <strong class="mainText">첫번째 댓글 작성</strong>
                                <span class="time">2018.7.23 17:33</span>
                            </p>
                            <strong class="pointItemValue">
                                5<span>P</span>
                            </strong>
                        </div>
                    </li>
                    <li class="pointMinus">
                        <div class="pointItem">
                            <span class="pointItemState">
                                사용
                            </span>
                            <p class="pointItemText">
                                <strong class="mainText">전문가 채팅방 입장</strong>
                                <span class="time">2018.7.23 17:33</span>
                            </p>
                            <strong class="pointItemValue">
                                20<span>P</span>
                            </strong>
                        </div>
                    </li>
                    <li>
                        <div class="pointItem">
                            <span class="pointItemState">
                                적립
                            </span>
                            <p class="pointItemText">
                                <strong class="mainText">Welcome Point</strong>
                                <span class="time">2018.7.23 17:33</span>
                            </p>
                            <strong class="pointItemValue">
                                297<span>P</span>
                            </strong>
                        </div>
                    </li>
                </ul>
                <button type="button" class="articleMore">더보기</button>
            </div>
        </div>

    </div>

    <!-- 쪽지 보내기 팝업 -->
    <div class="cPopup-mini popupSendLetter">
        <div class="cPopupInner-mini">
            <h3 class="cPopupTitle-mini">쪽지 보내기</h3>
            <button type="button" class="cPopupClose-mini jsSendLetterClose" aria-label="Close this popup">
                <span class="btnX small"></span>
            </button>
            <p class="cProfile">
                <span class="cProfileImg">
                    <img src="https://placekitten.com/640/360" alt="">
                </span>
                <span class="cProfileName">홍길동코인</span>
            </p>
            <form action="#">
                <fieldset>
                    <p class="inputField">
                        <label for="letterText" class="visuallyhidden">쪽지 내용을 입력해 주세요.</label>
                        <span class="input blueLine">
                            <textarea id="letterText" placeholder="쪽지 내용을 입력해 주세요."></textarea>
                        </span>
                        <strong class="message">Validation message</strong>
                    </p>
                    <div class="cPopupFooter-mini rowGroup">
                        <button type="button" class="cBtn line jsSendLetterClose">취소</button>
                        <button class="cBtn">보내기</button>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
    <!-- // 쪽지 보내기 팝업 -->

    <!-- 프로필 편집 팝업 -->
    <div class="cPopup-mini popupEditProfile">
        <div class="cPopupInner-mini">
            <h3 class="cPopupTitle-mini">프로필 편집</h3>
            <button type="button" class="cPopupClose-mini jsEditProfileClose" aria-label="Close this popup">
                <span class="btnX small"></span>
            </button>

            <form action="#">
                <fieldset>
                    <p class="editProfileImg">
                        <span class="cProfileImg size90">
                            <img src="https://placekitten.com/100/200" alt="">
                        </span>
                        <span class="editProfileUpload">
                            <button type="button" class="figment">
                                <span class="iconCamera"></span>
                                <span class="visuallyhidden">Edit my picture</span>
                            </button>
                            <input type="file" accept="image/*" class="inputFile">
                        </span>
                    </p>
                    <p class="editProfileName">자산관리사</p>
                    <p class="inputField">
                        <label for="introductionText" class="visuallyhidden">소개글을 입력해 주세요.</label>
                        <span class="input blueLine">
                            <textarea id="introductionText" placeholder="소개글을 입력해 주세요."></textarea>
                        </span>
                        <strong class="message">Validation message</strong>
                        <span class="length">0/150</span>
                    </p>
                    <div class="cPopupFooter-mini rowGroup">
                        <button type="button" class="cBtn line jsEditProfileClose">취소</button>
                        <button class="cBtn">확인</button>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
    <!-- // 프로필 편집 팝업 -->

    <!-- 팔로워 팝업 -->
    <div class="cPopup popupFollowList jsPopupFollower">
        <div class="cPopupInner">
            <h2 class="cPopupTitle">팔로워</h2>
            <button type="button" class="cPopupClose jsPopupClose" aria-label="Close this popup">
                <span class="btnX"></span>
            </button>

            <div class="cPopupBody">
                <!-- <p class="noData">팔로워가 없습니다.</p> -->
                <ul class="followList">
                    <li>
                        <span class="cProfileImg size45">
                            <!-- <img src="/chatbit/img/@temp-person.jpg" alt=""> -->
                        </span>
                        <p class="text">
                            <strong class="ellipsis name">GHDRLFEHD</strong>
                            <span class="ellipsis desc">투자 정보를 원하시는 분은 제 아이디로 채팅방 검색 해주세요.</span>
                        </p>
                        <div class="btnWrap">
                            <button type="button" class="btnToggleFollow cBtn2 fill following">팔로잉</button>
                        </div>
                    </li>
                    <li>
                        <span class="cProfileImg size45">
                            <img src="/chatbit/img/@temp-person.jpg" alt="">
                        </span>
                        <p class="text">
                            <strong class="ellipsis name">길도이 형님</strong>
                            <span class="ellipsis desc"></span>
                        </p>
                        <div class="btnWrap">
                            <button type="button" class="btnToggleFollow cBtn2 fill">팔로우</button>
                        </div>
                    </li>
                    <li>
                        <span class="cProfileImg size45">
                            <img src="/chatbit/img/@temp-person.jpg" alt="">
                        </span>
                        <p class="text">
                            <strong class="ellipsis name">코인아카데미</strong>
                            <span class="ellipsis desc">대박투자 인생한방</span>
                        </p>
                        <div class="btnWrap">
                            <button type="button" class="btnToggleFollow cBtn2 fill">팔로우</button>
                        </div>
                    </li>
                    <li>
                        <span class="cProfileImg size45">
                            <img src="/chatbit/img/@temp-person.jpg" alt="">
                        </span>
                        <p class="text">
                            <strong class="ellipsis name">리사</strong>
                            <span class="ellipsis desc">투자 정보를 원하시는 분은 제 아이디로 채팅방 검색해주세요.</span>
                        </p>
                        <div class="btnWrap">
                            <button type="button" class="btnToggleFollow cBtn2 fill following">팔로잉</button>
                        </div>
                    </li>
                    <li>
                        <span class="cProfileImg size45">
                            <img src="/chatbit/img/@temp-person.jpg" alt="">
                        </span>
                        <p class="text">
                            <strong class="ellipsis name">누구?</strong>
                            <span class="ellipsis desc">안얄랴쥼</span>
                        </p>
                        <div class="btnWrap">
                            <button type="button" class="btnToggleFollow cBtn2 fill">팔로우</button>
                        </div>
                    </li>
                    <li>
                        <span class="cProfileImg size45">
                            <img src="/chatbit/img/@temp-person.jpg" alt="">
                        </span>
                        <p class="text">
                            <strong class="ellipsis name">코인 고고씽~</strong>
                            <span class="ellipsis desc">투자 정보를 원하시는 분은 제 아이디로 채팅방 검색해주세요.</span>
                        </p>
                        <div class="btnWrap">
                            <button type="button" class="btnToggleFollow cBtn2 fill">팔로우</button>
                        </div>
                    </li>
                    <li>
                        <span class="cProfileImg size45">
                            <img src="/chatbit/img/@temp-person.jpg" alt="">
                        </span>
                        <p class="text">
                            <strong class="ellipsis name">만수르</strong>
                            <span class="ellipsis desc">블핑 짱!</span>
                        </p>
                        <div class="btnWrap">
                            <button type="button" class="btnToggleFollow cBtn2 fill">팔로우</button>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- // 팔로워 팝업 -->

    <!-- 팔로우 팝업 -->
    <div class="cPopup popupFollowList jsPopupFollow">
        <div class="cPopupInner">
            <h2 class="cPopupTitle">팔로우</h2>
            <button type="button" class="cPopupClose jsPopupClose" aria-label="Close this popup">
                <span class="btnX"></span>
            </button>

            <div class="cPopupBody">
                <p class="noData">팔로우가 없습니다.</p>
                <!-- <ul class="followList">
                    <li>
                        <span class="cProfileImg size45">
                            <img src="/chatbit/img/@temp-person.jpg" alt="">
                        </span>
                        <p class="text">
                            <strong class="ellipsis name">GHDRLFEHD</strong>
                            <span class="ellipsis_multiple desc">투자 정보를 원하시는 분은 제 아이디로 채팅방 검색 해주세요.</span>
                        </p>
                        <div class="btnWrap">
                            <button type="button" class="btnToggleFollow cBtn2 fill following">팔로잉</button>
                        </div>
                    </li>
                </ul> -->
            </div>
        </div>
    </div>
    <!-- // 팔로우 팝업 -->

    <hr class="h9">

    <jsp:include page="/chatbit_m/include/footer.jsp" flush="true"></jsp:include>

    <script>
        // 팔로우 버튼 클릭시 팔로잉
        $('.btnToggleFollow').click(function () {
            var $this = $(this);
            if (!$this.hasClass('following')) {
                $this.addClass('following');
                $this.text('팔로잉');
            } else {
                $this.removeClass('following');
                $this.text('팔로우');
            }
        });

        // 탭
        $('.myPageTabs').on('click', '.cTabItem', function (e) {
            e.preventDefault();
            var $this = $(this);
            var selectedId = $this.attr('href').split('#')[1];
            $this.attr('aria-selected', 'true').siblings().attr('aria-selected', 'false');
            $('#' + selectedId).show().siblings().hide();
        });

        // 쪽지 보내기 팝업 열기
        $('.jsLetterBtn').click(function (e) {
            e.preventDefault();
            dimOn();
            $('.popupSendLetter').fadeIn();
        });

        // 쪽지 보내기 팝업 닫기
        $('.jsSendLetterClose').click(function () {
            dimOff();
            $(this).parents('.cPopup-mini').fadeOut(200);
            $('#letterText').val('');
        });

        // 팔로워 팝업 열기
        $('.jsFollowerList').click(function () {
            $('.jsPopupFollower').fadeIn();
        });

        // 팔로우 팝업 열기
        $('.jsFollowList').click(function () {
            $('.jsPopupFollow').fadeIn();
        });

        // 프로필 편집 팝업 열기
        $('.jsProfileSetting').click(function (e) {
            e.preventDefault();
            dimOn();
            $('.popupEditProfile').fadeIn();
        });

        // 프로필 편집 팝업 닫기
        $('.jsEditProfileClose').click(function () {
            dimOff();
            $(this).parents('.cPopup-mini').fadeOut(200);
            $('#introductionText').val('');
        });

        $('.jsMyPostDelete').click(function () {
            confirm('작성하신 글이 삭제됩니다. 정말로 삭제하시겠습니까?작성하신 글이 삭제됩니다. 정말로 삭제하시겠습니까?');
        });

        // 회원신고 팝업 열기
        $('.jsMemberReport').click(function () {

            // /chatbit_m/include/popup.jsp -> html
            $('.popupReport').fadeIn();
        });

        $('.jsExchange').click(function () {
            $('.jsPopupExchange').fadeIn();
        });

        $(document).ready(function () {

            // 프로필 편집 textarea
            var $profileInput = $('#introductionText');

            $profileInput.focus(function () {
                $profileInput.closest('.inputField').addClass('active');
            })
            $profileInput.blur(function () {
                $profileInput.closest('.inputField').removeClass('active');
            });
        });

        // 기사글 제목 말줄임표
        $(function () {
            $('.ellipsis_label').each(function (i, element) {
                $(element).dotdotdot({
                    keep: '.labelWrap'
                });
            });
        });
    </script>

</body>

</html>