<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" %>
<html lang="ko">

<head>
    <title> CHATBIT - YMAX </title>
    <meta charset="utf-8" />
    <meta name="format-detection" content="telephone=no" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <link href="/chatbit_m/css/base.css" type="text/css" rel="stylesheet" />
    <link href="/chatbit_m/css/layout.css" type="text/css" rel="stylesheet" />
    <link href="/chatbit_m/css/common.css" type="text/css" rel="stylesheet" />
    <link href="/chatbit_m/css/board.css" type="text/css" rel="stylesheet" />
    <script src="/chatbit/js/1.10.1.jquery.min.js" type="text/javascript" charset="utf-8"></script>
</head>

<body>

    <jsp:include page="/chatbit_m/include/header.jsp" flush="true"></jsp:include>

    <div class="containerWrap">

        <jsp:include page="/chatbit_m/include/boardMenu.jsp" flush="true"></jsp:include>
        <script>
            $('.jsBoardLinks [data-value="1"]').trigger('click');
        </script>

        <!-- 
                hot: HOT
                recommend: 추천
                represent: 대표글 
            -->
        <div class="articleViewHeader">
            <h2 class="title">암호화폐 시세 모니터링이 가능한 마이토큰 앱, 투자 생태계의 재편을 꿈꾸다.</h2>
            <p class="info">
                <a href="#" class="source">블록미디어</a>
                <time class="time">2018.08.08</time>
                <span class="views">조회수 100</span>
            </p>
            <!-- // .info -->

            <div class="info2">
                <span class="cLikes">45</span>
                <span class="cComments">1234</span>
                <div class="articleExtraFnWrap">
                    <button type="button" class="btnExtra jsExtraFn">
                        <span class="visuallyhidden">더보기</span>
                    </button>
                    <div class="articleExtraFn">
                        <ul class="extraFnList">
                            <li class="fontResize">
                                가
                                <button type="button" class="btnZoomout">
                                    <span class="visuallyhidden">글씨축소</span>
                                </button>
                                <button type="button" class="btnZoomin">
                                    <span class="visuallyhidden">글씨확대</span>
                                </button>
                            </li>
                            <li>
                                <button type="button" class="extraFnItem jsShare">
                                    <span class="iconShare"></span>
                                    공유
                                </button>
                            </li>
                            <li>
                                <button type="button" class="extraFnItem jsBookmark">
                                    <span class="iconBookmark"></span>
                                    Bookmark
                                </button>
                            </li>
                            <li>
                                <button type="button" class="extraFnItem jsPostReport">
                                    <span class="iconReport"></span>
                                    신고
                                </button>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- // .info2 -->

            <div class="bubbleSns bubbleLayer">
                <button type="button" class="cPopupClose jsBubbleClose" aria-label="Close this popup">
                    <span class="btnX small"></span>
                </button>
                <dl>
                    <dt class="bubbleSnsTitle">SNS 공유하기</dt>
                    <dd class="bubbleSnsLinks">
                        <a href="#" class="snsItem fb">
                            <span>페이스북</span>
                        </a>
                        <a href="#" class="snsItem twitter">
                            <span>트위터</span>
                        </a>
                        <a href="#" class="snsItem naver">
                            <span>네이버</span>
                        </a>
                        <a href="#" class="snsItem story">
                            <span>카카오 스토리</span>
                        </a>
                    </dd>
                </dl>
                <dl class="urlCopyField">
                    <dt class="visuallyhidden">URL 복사하기</dt>
                    <dd>
                        <input type="text" class="urlValue" value="http://www.chatbit.co.kr/12354">
                        <button type="button" class="btnRound small urlCopyBtn jsCopy">URL복사</button>
                    </dd>
                </dl>
            </div>
            <!-- // .bubbleSns -->
        </div>
        <!-- // .articleViewHeader -->

        <div class="articleViewContents">
            <p>구글(Google)이 최근 암호화폐 앱에 대한 입장을 다시 발표했다.
                구글은 “우리는 직접 암호화폐를 채굴하는 앱을 허용하지 않는다. 다만, 암호화폐 채굴을 원격으로 관리하는 앱은 허용한다”라고 밝혔다. 그들의 발표를 통해 알 수 있듯이 클라우드 형태
                등의 원격 채굴과 관련된 애플리케이션은 플레이스토어에서 제거되지않을 예정이다.</p>
            <p>이로써 구글은 지난 4월 발표했던 ‘암호화폐 채굴 확장 금지’보다 더 엄격한 ‘암호화폐 채굴 앱 제거’ 정책을 내놓을 것으로 평가된다. 또한, 이는 지난 6월 애플(Apple)이 자사의
                앱스토어 및 스마트 기기에서 암호화폐 채굴 앱을 금지한 것과 맥을 같이한다.</p>
            <img src="https://www.fillmurray.com/640/360" alt="">
            <p style="font-size: 18px; font-weight: 500;"><span style="color: red; font-size: 24px;">또 다른</span> IT 대기업
                페이스북은?</p>
            <p>한편, 구글과 함께 IT 대기업으로 손꼽히는 페이스북은 최근 구글과는 다른 행보를 보이고 있다. 올해 초, 구글과 페이스북은 트위터 등과 더불어 ICO 및 암호화폐 광고를 금지하는 강경한
                정책을 시행하겠다고 밝힌 바 있다.</p>
            <p>하지만, 이후 페이스북은 광고 금지 정책과는 별개로 “자체 암호화폐 발행을 고려하고 있다”고 밝히고 코인베이스(Coinbase) 등 암호화폐 기업의 임원을 영입하는 등 암호화폐 산업에서
                활발한 행보를 보여왔다.</p>
        </div>
        <!-- // .articleViewContents -->

        <div class="articleViewButtons">
            <button type="button" class="cBtn2 iconLike jsLike">46</button>
            <button type="button" class="cBtn2 jsShowWrite">수정</button>
            <button type="button" class="cBtn2 jsPostDelete">삭제</button>
            <a href="./toktok.jsp" class="cBtn2">목록</a>
        </div>

        <div>
            <div class="commentHeader">
                <h3 class="title">댓글</h3>
                <p class="length">123</p>

                <!-- D: 댓글 새로고침 시 필요하면 사용하세요 -->
                <button type="button" class="btnRefresh"><span class="visuallyhidden">Refresh</span></button>
            </div>

            <!-- 댓글등록 -->
            <!-- D: toggle class = "login" -->
            <div class="commentRegister login">
                <!-- 로그인 전 -->
                <p class="beforeLogin">로그인을 해주세요.</p>

                <!-- 로그인 후 -->
                <form action="#" class="afterLogin">
                    <fieldset>
                        <textarea placeholder="타인에게 불쾌감을 주는 욕설이나 비하, 혐오 발언은  건전하고 아름다운 댓글 문화를 위해 관리자에 의해 삭제될 수 있습니다."></textarea>
                        <span class="length">
                            <span>0</span>/300 byte
                        </span>
                        <button class="register">등록</button>
                    </fieldset>
                </form>
            </div>
            <!-- // 댓글등록 -->

            <!-- 댓글 목록 -->
            <div class="comments">
                <ul class="commentList">
                    <li>
                        <!-- 수정할 때 editable 클래스명 추가 -->
                        <div class="commentItem">
                            <span class="avatarWrap">
                                <span class="avatar">
                                    <!-- <img src="https://fillmurray.com/100/50" alt=""> -->
                                </span>
                            </span>
                            <p class="info">
                                <strong class="name">본일 일 때만 수정 삭제 가능</strong>
                                <time class="time">2018.7.23 17:33</time>
                            </p>
                            <div class="commentAction">
                                <button type="button" class="commentActionBtn jsCommentFn active">
                                    <span class="visuallyhidden">더보기</span>
                                </button>
                                <div class="commentActionLayer">
                                    <ul class="commentActionList">
                                        <li>
                                            <button type="button" class="modify jsCommentModify">수정</button>
                                        </li>
                                        <li>
                                            <button type="button" class="delete jsCommentDelete">삭제</button>
                                        </li>
                                        <!-- <li>
                                            <button type="button" class="report jsCommentReport">신고</button>
                                        </li> -->
                                    </ul>
                                </div>
                            </div>
                            <div class="text">
                                <p class="textValue">
                                    엄지척
                                </p>
                                <textarea name="" id="" cols="30" rows="10">엄지척</textarea>
                                <span class="length">
                                    <span>0</span>/300 byte
                                </span>
                            </div>
                            <div class="buttons">
                                <button type="button" class="cBtn2 small jsCommentCancel">취소</button>
                                <button type="button" class="cBtn2 small jsCommentRegister">등록</button>
                            </div>
                        </div>
                    </li>
                    <li>
                        <!-- 수정할 때 editable 클래스명 추가 -->
                        <div class="commentItem">
                            <span class="avatarWrap">
                                <span class="avatar">
                                    <img src="https://fillmurray.com/100/50" alt="">
                                </span>
                            </span>
                            <p class="info">
                                <strong class="name">홍길동</strong>
                                <time class="time">2018.7.23 17:33</time>
                            </p>
                            <div class="commentAction">
                                <button type="button" class="commentActionBtn jsCommentFn active">
                                    <span class="visuallyhidden">더보기</span>
                                </button>
                                <div class="commentActionLayer">
                                    <ul class="commentActionList">
                                        <!-- <li>
                                            <button type="button" class="modify jsCommentModify">수정</button>
                                        </li>
                                        <li>
                                            <button type="button" class="delete jsCommentDelete">삭제</button>
                                        </li> -->
                                        <li>
                                            <button type="button" class="report jsCommentReport">신고</button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="text">
                                <p class="textValue">
                                    세계 최대 암호화폐(가상화폐) 채굴기업 비트메인이 사업 투명성 제고를 위한 새로운 정책을 발표했다.27일 비트코인뉴스에 따르면 최근 비트코인의
                                    ASIC를 개발・판매하는 비트메인은 정보 공개를 통해 사업의 투명성을 높이는 방침을 발표한 것으로 알려졌다.
                                </p>
                                <textarea name="" id="" cols="30" rows="10">세계 최대 암호화폐(가상화폐) 채굴기업 비트메인이 사업 투명성 제고를 위한 새로운 정책을 발표했다.27일 비트코인뉴스에 따르면 최근 비트코인의
                                    ASIC를 개발・판매하는 비트메인은 정보 공개를 통해 사업의 투명성을 높이는 방침을 발표한 것으로 알려졌다.</textarea>
                                <span class="length">
                                    <span>0</span>/300 byte
                                </span>
                            </div>
                            <div class="buttons">
                                <button type="button" class="cBtn2 small jsCommentCancel">취소</button>
                                <button type="button" class="cBtn2 small jsCommentRegister">등록</button>
                            </div>
                        </div>
                    </li>
                    <li>
                        <!-- 수정할 때 editable 클래스명 추가 -->
                        <div class="commentItem editable">
                            <span class="avatarWrap">
                                <span class="avatar">
                                    <img src="https://fillmurray.com/100/50" alt="">
                                </span>
                            </span>
                            <p class="info">
                                <strong class="name">홍길동</strong>
                                <time class="time">2018.7.23 17:33</time>
                            </p>
                            <div class="commentAction">
                                <button type="button" class="commentActionBtn jsCommentFn">
                                    <span class="visuallyhidden">더보기</span>
                                </button>
                                <div class="commentActionLayer">
                                    <ul class="commentActionList">
                                        <li>
                                            <button type="button" class="modify jsCommentModify">수정</button>
                                        </li>
                                        <li>
                                            <button type="button" class="delete jsCommentDelete">삭제</button>
                                        </li>
                                        <li>
                                            <button type="button" class="report jsCommentReport">신고</button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="text">
                                <p class="textValue">
                                    세계 최대 암호화폐(가상화폐) 채굴기업 비트메인이 사업 투명성 제고를 위한 새로운 정책을 발표했다.27일 비트코인뉴스에 따르면 최근 비트코인의
                                    ASIC를 개발・판매하는 비트메인은 정보 공개를 통해 사업의 투명성을 높이는 방침을 발표한 것으로 알려졌다.
                                </p>
                                <textarea name="" id="" cols="30" rows="10">세계 최대 암호화폐(가상화폐) 채굴기업 비트메인이 사업 투명성 제고를 위한 새로운 정책을 발표했다.27일 비트코인뉴스에 따르면 최근 비트코인의
                                    ASIC를 개발・판매하는 비트메인은 정보 공개를 통해 사업의 투명성을 높이는 방침을 발표한 것으로 알려졌다.</textarea>
                                <span class="length">
                                    <span>0</span>/300 byte
                                </span>
                            </div>
                            <div class="buttons">
                                <button type="button" class="cBtn2 small jsCommentCancel">취소</button>
                                <button type="button" class="cBtn2 small jsCommentRegister">등록</button>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            <!-- // 댓글 목록 -->
            <button type="button" class="articleMore">더보기</button>
        </div>

        <!-- 페이징 -->
        <div class="viewPagingWrap">
            <a href="#" class="viewPaging disabled">
                <p>
                    <span class="direction">이전글</span>
                    <span class="title ellipsis">이전글이 없습니다.</span>
                </p>
            </a>
            <a href="./toktokView.jsp" class="viewPaging">
                <p>
                    <span class="direction">다음글</span>
                    <span class="title ellipsis">감사는 Binance-Listed ERC20 토큰에batchOverflow'의 건강 상태 정리</span>
                </p>
            </a>
        </div>
        <!-- 페이징 -->
    </div>

    <jsp:include page="/chatbit_m/include/footer.jsp" flush="true"></jsp:include>

    <script>
        // .jsExtraFn: 글씨크기조정, 공유, 북마크, 신고 - 레이어 토글
        // .jsCommentFn: 댓글 삭제, 수정, 신고 - 레이어 토글
        $('.jsExtraFn, .jsCommentFn').click(function () {
            var $this = $(this);
            if (!$this.hasClass('active')) {
                $this.addClass('active');
            } else {
                $this.removeClass('active');
            }
        });

        // 댓글 삭제
        $('.jsCommentDelete').click(function () {
            confirm('댓글을 삭제하시겠습니까?');
        });

        // 댓글 수정
        $('.jsCommentModify').click(function () {
            var $this = $(this);
            $this.closest('.commentItem').addClass('editable');
            $this.closest('.commentAction').find('.jsCommentFn').removeClass('active');
        });

        // 댓글 수정: 취소 저장
        $('.jsCommentCancel, .jsCommentRegister').click(function () {
            $(this).closest('.commentItem').removeClass('editable');
        });

        // 댓글 신고
        $('.jsCommentReport').click(function () {
            confirm('신고한 게시글 또는 댓글은 운영규정에 따라 삭제 또는 유지될 수 있습니다. 신고하시겠습니까?');
            $(this).closest('.commentItem').removeClass('editable');
        });

        // 작은팝업 - sns 공유하기 팝업 닫기
        $('.jsBubbleClose').click(function (e) {
            dimOff();
            $(this).parent().fadeOut('fast');
        });

        // 공유하기 팝업 열기
        $('.jsShare').click(function () {
            $(this).parents('.articleExtraFnWrap').find('.jsExtraFn').removeClass('active');
            dimOn();
            $('.bubbleSns').fadeIn('fast');
        });

        // url 복사하기
        $('.jsCopy').click(function () {
            var pageUrl = document.querySelector('.urlValue');
            pageUrl.select();
            document.execCommand("copy");
            alert('URL이 복사 되었습니다.');
        });

        // 북마크, 추천 버튼 active
        $('.jsBookmark, .jsLike').click(function () {
            var $this = $(this);
            if ($this.hasClass('active')) {
                $this.removeClass('active');
            } else {
                $this.addClass('active');
            }
        });

        // 게시글 삭제
        $('.jsPostDelete').click(function (e) {
            e.preventDefault();
            confirm('작성하신 글이 삭제됩니다. 정말로 삭제하시겠습니까?');
        });

        // 게시글 신고
        $('.jsPostReport').click(function () {
            confirm('신고한 게시글은 운영규정에 따라 삭제 또는 유지될 수 있습니다. 신고하시겠습니까?');
        });

        // 게시글 수정
        $('.jsShowWrite').click(function () {
            $('.popupWritePost').fadeIn(200);
        });

        // 게시글 폰트사이즈 조정
        function changeFontSize(zoomSize) {
            $(".articleViewContents").find('*').each(function (i, element) {
                var $el = $(element);
                var currentFontSize = parseInt($el.css('fontSize'));
                $el.css('fontSize', currentFontSize + Number(zoomSize));
            });
        }

        // 게시글 폰트사이즈 조정
        $('.fontResize').on('click', 'button', function () {
            var size = $(this).hasClass('btnZoomin') ? '+2' : '-2';
            changeFontSize(size);
        });

        // body 클릭 시 레이어 닫기
        $('body').on('click, touchstart', function (e) {
            e.stopPropagation();

            var $postExtraFn = $('.articleExtraFnWrap');
            if ($postExtraFn.find('.jsExtraFn').hasClass('active')) {
                if (!$postExtraFn.is(e.target) && $postExtraFn.has(e.target).length === 0) {
                    $postExtraFn.find('.jsExtraFn').removeClass('active');
                }
            }

            var $commentFn = $('.commentAction');
            $commentFn.each(function (i, element) {
                var $el = $(element);
                if ($el.find('.jsCommentFn').hasClass('active')) {
                    if (!$el.is(e.target) && $el.has(e.target).length === 0) {
                        $el.find('.jsCommentFn').removeClass('active');
                    }
                }
            });
        });
    </script>

</body>

</html>