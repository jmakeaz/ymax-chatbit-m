<!DOCTYPE html>
<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" %>
<html lang="ko">

<head>
    <title> CHATBIT - YMAX </title>
    <meta charset="utf-8" />
    <meta name="format-detection" content="telephone=no" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <link href="/chatbit_m/css/base.css" type="text/css" rel="stylesheet" />
    <link href="/chatbit_m/css/layout.css" type="text/css" rel="stylesheet" />
    <link href="/chatbit_m/css/common.css" type="text/css" rel="stylesheet" />
    <link href="/chatbit_m/game/game.css" type="text/css" rel="stylesheet" />
    <script src="/chatbit/js/1.10.1.jquery.min.js" type="text/javascript" charset="utf-8"></script>
</head>

<body>

    <jsp:include page="/chatbit_m/include/header.jsp" flush="true"></jsp:include>

    <!-- <button type="button" id="jsTimer">타이머 팝업 열기</button> -->
    <button type="button" id="jsShowResultGood">[투자성공팝업]</button>
    <button type="button" id="jsShowResultBad">[투자실패팝업]</button>

    <div class="containerWrap">

        <h2 class="pageTitle">모의투자</h2>

        <div class="game-wrap">
            <div class="game-header">
                <h1 class="game-header-heading">
                    <img src="img/en/textHeading.png" alt="The god of short trading" width="270">
                </h1>
            </div>

            <div class="game-state">
                <!-- 공지사항 -->
                <p class="game-state-notice ellipsis">
                    <strong class="game-state-notice-label">Notice</strong>가상화폐 거래소 바이낸스에 BTC/USDT 기준 가상화폐 거래조 조심
                </p>

                <!-- 도움말 -->
                <div class="game-guide">
                    <button type="button" class="game-guide-button jsGuideButton">Help</button>
                </div>

                <div class="game-state-graph">
                    <img src="img/@tempGraph.jpg" alt="" width="289">
                </div>
            </div>

            <div class="game-action">
                <div class="game-action-box">
                    <div class="game-action-have-wrap">
                        <dl class="game-action-have">
                            <dt>Successful investment CGP :</dt>
                            <dd>10,000,000</dd>
                        </dl>
                        <button type="button" class="game-exchange-button jsExchange">
                            <span class="game-exchange-button-text">Exchange CGP<span class="iconTrade"></span></span>
                        </button>
                    </div>

                    <dl class="game-action-invest">
                        <dt>Investment amount</dt>
                        <dd>
                            <input type="text" name="" id="" value="0" class="game-action-invest-input">CGP
                        </dd>
                    </dl>
                    <div class="game-action-buttons">
                        <button type="button" class="game-action-button">+ 100</button>
                        <button type="button" class="game-action-button">+ 1,000</button>
                        <button type="button" class="game-action-button">+ 10,000</button>
                    </div>

                    <dl class="game-action-sucess">
                        <dt>Investment amount :</dt>
                        <dd>0 CGP</dd>
                    </dl>
                </div>

                <div class="game-invest-buttons">
                    <button type="button" class="game-invest-up-button jsInvestUp">
                        <span class="inner"><img src="./img/en/buttonInvest1-text.png" alt="Invest in increase" width="125"></span>
                    </button>

                    <button type="button" class="game-invest-down-button jsInvestDown">
                        <span class="inner"><img src="./img/en/buttonInvest2-text.png" alt="Invest in decline" width="119"></span>
                    </button>
                    <div class="game-invest-wating jsInvestUpLayer">
                        <p class="game-invest-wating-text1">You invested in increase.</p>
                        <p class="game-invest-wating-text2">
                            Remaining time
                            <strong class="second jsCloseTimer">29</strong>
                            seconds
                        </p>
                    </div>
                    <div class="game-invest-wating down jsInvestDownLayer">
                        <p class="game-invest-wating-text1">You invested in decline.</p>
                        <p class="game-invest-wating-text2">
                            Remaining time
                            <strong class="second jsCloseTimer">29</strong>
                            seconds
                        </p>
                    </div>
                </div>
            </div>

            <div class="game-history">
                <h2 class="game-history-title"><img src="./img/en/textStory.png" alt="My investment history" width="164"></h2>
                <table class="game-history-table">
                    <thead>
                        <tr>
                            <th>Time</th>
                            <th>Details</th>
                            <th>Amount</th>
                            <th>Win Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                15:03<br />
                                2018-10-08
                            </td>
                            <td>
                                <span class="game-history-up">Rise</span>
                            </td>
                            <td>
                                진행중<br />
                                <span class="game-history-money">6,501.241456</span>
                            </td>
                            <td>
                                18,000
                            </td>
                        </tr>
                        <tr>
                            <td>
                                15:03<br />
                                2018-10-08
                            </td>
                            <td>
                                <span class="game-history-down">Decline</span>
                            </td>
                            <td>
                                진행중<br />
                                <span class="game-history-money">6,501.241456</span>
                            </td>
                            <td>
                                1,000
                            </td>
                        </tr>
                        <tr>
                            <td>
                                15:03<br />
                                2018-10-08
                            </td>
                            <td>
                                <span class="game-history-up">Rise</span>
                            </td>
                            <td>
                                진행중<br />
                                <span class="game-history-money">6,501.241456</span>
                            </td>
                            <td>
                                1,000
                            </td>
                        </tr>
                        <tr>
                            <td>
                                15:03<br />
                                2018-10-08
                            </td>
                            <td>
                                <span class="game-history-down">Decline</span>
                            </td>
                            <td>
                                진행중<br />
                                <span class="game-history-money">6,501.241456</span>
                            </td>
                            <td>
                                1,000
                            </td>
                        </tr>
                        <tr>
                            <td>
                                15:03<br />
                                2018-10-08
                            </td>
                            <td>
                                <span class="game-history-up">Rise</span>
                            </td>
                            <td>
                                진행중<br />
                                <span class="game-history-money">6,501.241456</span>
                            </td>
                            <td>
                                1,000
                            </td>
                        </tr>
                    </tbody>
                </table>
                <button type="button" class="articleMore">More</button>
            </div>

            <div class="game-notice">
                <h2 class="game-notice-title">알아두세요</h2>
                <ul class="game-notice-list">
                    <li>단타의 신은 CGP로만 참여할 수 있으며 CGP는 CBP를 교환하여 얻을 수 있습니다. </li>
                    <li>CBP는 향후 @@코인으로 환전할 수 있도록 준비중입니다. </li>
                    <li>단타의 신은 오전 0시를 기준으로 회차가 초기화되며 매일 5???분에 한번씩 총 288회의 게임을 할 수 있습니다. </li>
                    <li>회차 시작 후 3분이 지나면 투자할 수 없습니다. </li>
                    <li>예상이 적중하면 획득하는 CGP 중 원금을 제외한 수익의 3%를 제하고 지급합니다.</li>
                </ul>
            </div>

        </div>
        <!-- // .game-wrap -->
    </div>

    <div class="cPopup-mini popupGameGuide">
        <div class="cPopupInner-mini">
            <h3 class="cPopupTitle-mini">HELP</h3>
            <button type="button" class="cPopupClose-mini jsGameGuideClose" aria-label="Close this popup">
                <span class="btnX small"></span>
            </button>

            <ul class="game-guide-list">
                <li>To participate in the God of investment, please exchange CBP to CGP.</li>
                <li>Turn lasts 1 minute, 30 seconds provided for investment decision and results each.</li>
                <li>How to invest
                    <ol>
                        <li>Set the investment amount. </li>
                        <li>Predict whether the price will rise or decline in the current instance of the game and
                            invest. </li>
                        <li>If you were correct, you will obtain the reward. The invested amount will not be returned
                            if
                            you were wrong.</li>
                    </ol>
                </li>
                <li>Please invest with caution. The chart gets updated every second.</li>
            </ul>
        </div>
    </div>

    <div class="game-result-popup game-result-good">
        <div class="game-result-inner">
            <h1 class="game-result-title">
                <img src="./img/en/textGood.png" alt="Congratulations on correct prediction!" width="199">
            </h1>

            <p class="game-result-start">Investment Point : 6,501.24 <span class="game-result-up-text">Rise</span></p>
            <p class="game-result-start-value">Amount invested : 6,500 CGP</p>

            <p class="game-result-end">Ending Point : 6,501.24</p>
            <p class="game-result-end-value">Win Amount : 6,500 CGP</p>

            <button type="button" class="game-result-close jsCloseResult">Close</button>
            <p class="game-result-auto-close">Auto close in <span class="jsCloseTimer">3</span> seconds.</p>
        </div>
    </div>
    <!-- // .game-result-good -->

    <div class="game-result-popup game-result-bad">
        <div class="game-result-inner">
            <h1 class="game-result-title"><img src="./img/en/textBad.png" alt="Wrong prediction try again!" width="157"></h1>

            <p class="game-result-start">Investment Point : 6,501.24 <span class="game-result-down-text">Decline</span></p>
            <p class="game-result-start-value">Amount invested : 6,500 CGP</p>

            <p class="game-result-end">Ending Point : 6,501.24</p>
            <p class="game-result-end-value">Win Amount : 6,500 CGP</p>

            <button type="button" class="game-result-close jsCloseResult">Close</button>
            <p class="game-result-auto-close">Auto close in <span class="jsCloseTimer">3</span> seconds.</p>
        </div>
    </div>
    <!-- // .game-result-bad -->

    <jsp:include page="/chatbit_m/include/footer.jsp" flush="true"></jsp:include>
    <script>
        (function () {

            // 도움말 팝업 열기
            $('.jsGuideButton').click(function () {
                var $popup = $('.popupGameGuide');
                $popup.fadeIn('slow');
                dimOn();
                return false;
            });

            // 도움말 팝업 닫기
            $('.jsGameGuideClose').click(function () {
                dimOff();
                $(this).parents('.cPopup-mini').fadeOut(200);
            });

            // 상승에 투자하기 클릭했을 때 레이어
            $('.jsInvestUp').click(function () {
                var $layer = $('.jsInvestUpLayer');
                $layer.fadeIn();
                closeCount($layer, $layer.find('.jsCloseTimer'), 30);
            });

            // 상승에 투자하기 클릭했을 때 레이어
            $('.jsInvestDown').click(function () {
                var $layer = $('.jsInvestDownLayer');
                $layer.fadeIn();
                closeCount($layer, $layer.find('.jsCloseTimer'), 30);
            });

            $('.jsExchange').click(function () {
                $('.jsPopupExchange').fadeIn();
            });

            // 임시 - 투자성공 팝업 show
            $('#jsShowResultGood').click(function () {
                var $popup = $('.game-result-good');
                $popup.fadeIn('slow');
                dimOn();
                closeCount($popup, $popup.find('.jsCloseTimer'), 3);
                return false;
            });

            // 임시 - 투자실패 팝업 show
            $('#jsShowResultBad').click(function () {
                var $popup = $('.game-result-bad');
                $popup.fadeIn('slow');
                dimOn();
                closeCount($popup, $popup.find('.jsCloseTimer'), 3);
                return false;
            });

            // 투자결과 팝업 hide
            $('.jsCloseResult').click(function () {
                $(this).closest('.game-result-popup').fadeOut('slow');
                dimOff();
                clearInterval(changeTime);
                return false;
            });

            // 투자결과 팝업 3초뒤에 자동 닫기
            var changeTime;

            function closeCount(popup, timer, second) {
                var timeLimit = second;
                var $timeText = timer;
                $timeText.text(timeLimit);

                changeTime = setInterval(function () {
                    timeLimit -= 1
                    $timeText.text(timeLimit);

                    if (timeLimit === 0) {
                        clearInterval(changeTime);
                        popup.fadeOut('slow');
                        if ($('body').has('.dim')) {
                            dimOff();
                        }
                        return false;
                    }
                }, 1000)
            }
        })();
    </script>

</body>

</html>