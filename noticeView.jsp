<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" %>
<html lang="ko">

<head>
    <title> CHATBIT - YMAX </title>
    <meta charset="utf-8" />
    <meta name="format-detection" content="telephone=no" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <link href="/chatbit_m/css/base.css" type="text/css" rel="stylesheet" />
    <link href="/chatbit_m/css/layout.css" type="text/css" rel="stylesheet" />
    <link href="/chatbit_m/css/common.css" type="text/css" rel="stylesheet" />
    <link href="/chatbit_m/css/board.css" type="text/css" rel="stylesheet" />
    <script src="/chatbit/js/1.10.1.jquery.min.js" type="text/javascript" charset="utf-8"></script>
</head>

<body>

    <jsp:include page="/chatbit_m/include/header.jsp" flush="true"></jsp:include>

    <div class="containerWrap">

        <jsp:include page="/chatbit_m/include/customerMenu.jsp" flush="true"></jsp:include>
        <script>
            $('.jsBoardLinks [data-value="1"]').trigger('click');
        </script>

        <!-- 
                hot: HOT
                recommend: 추천
                represent: 대표글 
            -->
        <div class="articleViewHeader typeSimple">
            <h2 class="title">고객센터 시스템 점검 안내</h2>
            <p class="info">
                <span class="time">2018.08.09 11:34</span>
            </p>
            <!-- // .info -->

            <div class="info2">
                <div class="articleExtraFnWrap">
                    <button type="button" class="btnExtra jsExtraFn">
                        <span class="visuallyhidden">더보기</span>
                    </button>
                    <div class="articleExtraFn">
                        <ul class="extraFnList">
                            <li class="fontResize">
                                가
                                <button type="button" class="btnZoomout">
                                    <span class="visuallyhidden">글씨축소</span>
                                </button>
                                <button type="button" class="btnZoomin">
                                    <span class="visuallyhidden">글씨확대</span>
                                </button>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- // .info2 -->

        </div>
        <!-- // .articleViewHeader -->

        <div class="articleViewContents">
            <p>
                안녕하세요 Chatbit입니다.<br />
                보다 안정적인 서비스 제공을 위해 고객센터 시스템 점검 작업이 진행될 예정입니다. 작업이 진행되는 동안 기존 문의내역 확인 및 문의접수를 하실 수 없습니다.
            </p>

            <p>
                작업일시 : 2018년 8월 10일 00:00~05:00<br />
                제한 서비스 : 고객센터 내 문의하기, 문의내역 확인
            </p>

            <p>
                이용에 불편을 드려 죄송합니다. 점검 후 원활한 서비스 환경으로 찾아 뵙겠습니다.
            </p>
        </div>
        <!-- // .articleViewContents -->

        <div class="articleViewButtons">
            <a href="./notice.jsp" class="cBtn2">목록</a>
        </div>

        <!-- 페이징 -->
        <div class="viewPagingWrap">
            <a href="#" class="viewPaging disabled">
                <p>
                    <span class="direction">이전글</span>
                    <span class="title ellipsis">이전글이 없습니다.</span>
                </p>
            </a>
            <a href="./noticeView.jsp" class="viewPaging">
                <p>
                    <span class="direction">다음글</span>
                    <span class="title ellipsis">추석 연휴 고객센터 휴무 안내</span>
                </p>
            </a>
        </div>
        <!-- 페이징 -->
    </div>

    <jsp:include page="/chatbit_m/include/footer.jsp" flush="true"></jsp:include>

    <script>
        // .jsExtraFn: 글씨크기조정, 공유, 북마크, 신고 - 레이어 토글
        // .jsCommentFn: 댓글 삭제, 수정, 신고 - 레이어 토글
        $('.jsExtraFn, .jsCommentFn').click(function () {
            var $this = $(this);
            if (!$this.hasClass('active')) {
                $this.addClass('active');
            } else {
                $this.removeClass('active');
            }
        });

        // 댓글 삭제
        $('.jsCommentDelete').click(function () {
            confirm('댓글을 삭제하시겠습니까?');
        });

        // 댓글 수정
        $('.jsCommentModify').click(function () {
            var $this = $(this);
            $this.closest('.commentItem').addClass('editable');
            $this.closest('.commentAction').find('.jsCommentFn').removeClass('active');
        });

        // 댓글 수정: 취소 저장
        $('.jsCommentCancel, .jsCommentRegister').click(function () {
            $(this).closest('.commentItem').removeClass('editable');
        });

        // 댓글 신고
        $('.jsCommentReport').click(function () {
            confirm('신고한 게시글 또는 댓글은 운영규정에 따라 삭제 또는 유지될 수 있습니다. 신고하시겠습니까?');
            $(this).closest('.commentItem').removeClass('editable');
        });

        // 작은팝업 - sns 공유하기 팝업 닫기
        $('.jsBubbleClose').click(function (e) {
            dimOff();
            $(this).parent().fadeOut('fast');
        });

        // 공유하기 팝업 열기
        $('.jsShare').click(function () {
            $(this).parents('.articleExtraFnWrap').find('.jsExtraFn').removeClass('active');
            dimOn();
            $('.bubbleSns').fadeIn('fast');
        });

        // url 복사하기
        $('.jsCopy').click(function () {
            var pageUrl = document.querySelector('.urlValue');
            pageUrl.select();
            document.execCommand("copy");
            alert('URL이 복사 되었습니다.');
        });

        // 북마크, 추천 버튼 active
        $('.jsBookmark, .jsLike').click(function () {
            var $this = $(this);
            if ($this.hasClass('active')) {
                $this.removeClass('active');
            } else {
                $this.addClass('active');
            }
        });

        // 게시글 삭제
        $('.jsPostDelete').click(function (e) {
            e.preventDefault();
            confirm('작성하신 글이 삭제됩니다. 정말로 삭제하시겠습니까?');
        });

        // body 클릭 시 레이어 닫기
        $('body').click(function (e) {
            e.stopPropagation();

            var $postExtraFn = $('.articleExtraFnWrap');
            if ($postExtraFn.find('.jsExtraFn').hasClass('active')) {
                if (!$postExtraFn.is(e.target) && $postExtraFn.has(e.target).length === 0) {
                    $postExtraFn.find('.jsExtraFn').removeClass('active');
                }
            }

            var $commentFn = $('.commentAction');
            $commentFn.each(function (i, element) {
                var $el = $(element);
                if ($el.find('.jsCommentFn').hasClass('active')) {
                    if (!$el.is(e.target) && $el.has(e.target).length === 0) {
                        $el.find('.jsCommentFn').removeClass('active');
                    }
                }
            });
        });
    </script>

</body>

</html>