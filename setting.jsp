<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" %>
<html lang="ko">

<head>
    <title> CHATBIT - YMAX </title>
    <meta charset="utf-8" />
    <meta name="format-detection" content="telephone=no" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <link href="/chatbit_m/css/base.css" type="text/css" rel="stylesheet" />
    <link href="/chatbit_m/css/layout.css" type="text/css" rel="stylesheet" />
    <link href="/chatbit_m/css/common.css" type="text/css" rel="stylesheet" />
    <link href="/chatbit_m/css/mypage.css" type="text/css" rel="stylesheet" />
    <script src="/chatbit/js/1.10.1.jquery.min.js" type="text/javascript" charset="utf-8"></script>
</head>

<body>

    <jsp:include page="/chatbit_m/include/header.jsp" flush="true"></jsp:include>

    <div class="containerWrap">

        <h2 class="pageTitle">알림설정</h2>

        <form action="#" class="setNotice">
            <fieldset class="setNoticeGroup">
                <p class="inputField setNoticeEmail">
                    <label for="a" class="label">이메일 알림</label>
                    <span class="input">
                        <input type="text" id="a" value="my-email@chatbit.com">
                    </span>
                    <strong class="message">사용할 수 없습니다.</strong>
                    <button type="button" class="cBtn confirm">이메일 인증</button>
                </p>
                <p class="setNoticeText">이메일 인증 후 이메일을 변경하실 수 있습니다. </p>

                <p class="setNoticeField setNoticeAd">
                    <strong class="name">홍보메일 수신</strong>
                    <span class="cSwitch toggleBtn">
                        <input type="checkbox" name="" id="emailAd" class="visuallyhidden">
                        <label for="emailAd" class="cSwitchLabel">
                        </label>
                    </span>
                </p>
                <p class="setNoticeText">
                    수신 동의하실 경우 Chatbit의 유용한 정보를 담은 뉴스레터와 코인토스 주간리포트, [거래소명]에 상장된 신규 코인에 대한 정보를 받아보실 수 있습니다.
                </p>
            </fieldset>
            <fieldset class="setNoticeGroup">
                <p class="setNoticeField setNoticeFollow">
                    <strong class="name">챗비트 팔로우 알림</strong>
                    <span class="cSwitch toggleBtn">
                        <input type="checkbox" name="" id="followNoti" class="visuallyhidden">
                        <label for="followNoti" class="cSwitchLabel">
                        </label>
                    </span>
                </p>

                <p class="setNoticeField">
                    <strong class="name">챗비트 게시글 알림</strong>
                    <span class="cSwitch toggleBtn">
                        <input type="checkbox" name="" id="postNoti" class="visuallyhidden" checked>
                        <label for="postNoti" class="cSwitchLabel">
                        </label>
                    </span>
                </p>

                <p class="setNoticeField">
                    <strong class="name">챗비트 댓글/추천 알림</strong>
                    <span class="cSwitch toggleBtn">
                        <input type="checkbox" name="" id="commentNoti" class="visuallyhidden">
                        <label for="commentNoti" class="cSwitchLabel">
                        </label>
                    </span>
                </p>
            </fieldset>
        </form>

    </div>

    <jsp:include page="/chatbit_m/include/footer.jsp" flush="true"></jsp:include>

</body>

</html>