<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" %>
<html lang="ko">

<head>
    <title> CHATBIT - YMAX </title>
    <meta charset="utf-8" />
    <meta name="format-detection" content="telephone=no" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <link href="/chatbit_m/css/base.css" type="text/css" rel="stylesheet" />
    <link href="/chatbit_m/css/layout.css" type="text/css" rel="stylesheet" />
    <link href="/chatbit_m/css/common.css" type="text/css" rel="stylesheet" />
    <link href="/chatbit_m/css/board.css" type="text/css" rel="stylesheet" />
    <script src="/chatbit/js/1.10.1.jquery.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="/chatbit_m/js/jquery.dotdotdot.js" type="text/javascript" charset="utf-8"></script>
</head>

<body>

    <jsp:include page="/chatbit_m/include/header.jsp" flush="true"></jsp:include>

    <div class="containerWrap">

        <jsp:include page="/chatbit_m/include/customerMenu.jsp" flush="true"></jsp:include>
        <script>
            $('.jsBoardLinks [data-value="1"]').trigger('click');
        </script>

        <!-- article list -->
        <div class="noticeBoard">
            <ul class="noticeList">
                <li>
                    <a href="./noticeView.jsp" class="noticeItem">
                        <p class="noticeItemTitle ellipsis_multiple">고객센터 시스템 점검 안내 고객센터 시스템 점검 안내 고객센터 시스템 점검 안내</p>
                        <span class="noticeItemTime">2018.08.09</span>
                    </a>
                </li>
                <li>
                    <a href="./noticeView.jsp" class="noticeItem">
                        <p class="noticeItemTitle ellipsis_multiple">추석 연휴 고객센터 휴무 안내</p>
                        <span class="noticeItemTime">2018.08.09</span>
                    </a>
                </li>
                <li>
                    <a href="./noticeView.jsp" class="noticeItem">
                        <p class="noticeItemTitle ellipsis_multiple">회원정보 약관 변경 안내</p>
                        <span class="noticeItemTime">2018.08.09</span>
                    </a>
                </li>
                <li>
                    <a href="./noticeView.jsp" class="noticeItem">
                        <p class="noticeItemTitle ellipsis_multiple">단타의 신 시스템 점검 안내</p>
                        <span class="noticeItemTime">2018.08.09</span>
                    </a>
                </li>
                <li>
                    <a href="./noticeView.jsp" class="noticeItem">
                        <p class="noticeItemTitle ellipsis_multiple">봄맞이 이벤트 안내</p>
                        <span class="noticeItemTime">2018.08.09</span>
                    </a>
                </li>
            </ul>
        </div>
        <!-- article list -->
        <button type="button" class="articleMore">더보기</button>
    </div>

    <hr class="h9">

    <jsp:include page="/chatbit_m/include/footer.jsp" flush="true"></jsp:include>

</body>

</html>