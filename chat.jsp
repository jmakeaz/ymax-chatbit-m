<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" %>
<html lang="ko">

<head>
    <title> CHATBIT - YMAX </title>
    <meta charset="utf-8" />
    <meta name="format-detection" content="telephone=no" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <link href="/chatbit_m/css/base.css" type="text/css" rel="stylesheet" />
    <link href="/chatbit_m/css/layout.css" type="text/css" rel="stylesheet" />
    <link href="/chatbit_m/css/common.css" type="text/css" rel="stylesheet" />
    <link href="/chatbit_m/css/chat.css" type="text/css" rel="stylesheet" />
    <script src="/chatbit/js/1.10.1.jquery.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="/chatbit/js/slick.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="/chatbit_m/js/jquery.dotdotdot.js" type="text/javascript" charset="utf-8"></script>
</head>

<body>

    <jsp:include page="/chatbit_m/include/header.jsp" flush="true"></jsp:include>

    <div class="containerWrap">

        <h2 class="pageTitle">채팅</h2>

        <div class="chatSliderWrap">
            <div class="chatSlider expertChat jsChatSlider">

                <!-- item -->
                <div class="chatSliderItem">
                    <div class="inner">
                        <h3 class="message">
                            코인 전문가에게 물어보세요!
                        </h3>
                        <p class="profile">
                            <span class="profileImg">
                                <img src="/chatbit_m/img/@temp-man.png" alt="">
                            </span>
                            <span class="profileText">
                                <span class="profileName ellipsis">코인전문가</span>
                                <span class="profileDesc ellipsis_multiple">
                                    투자가치가 있는 신규코인에 대한 정보를 나누고 싶습니다. 채팅/쪽지 환영입니다.
                                </span>
                            </span>
                        </p>
                        <dl class="roomInfo">
                            <dt class="roomName ellipsis">코인 시장 분석과 에어드랍 이벤트 안내</dt>
                            <dd class="roomCategory ellipsis">코인시세 > 이더리움클래식</dd>
                            <dd class="roomDesc">
                                <p class="ellipsis_multiple">투자 가치가 있는 신규코인에 대한 정보를 나누고 싶습니다. 새로운 코인에 관심있으신 분들 모두
                                    환영합니다.</p>
                                <a href="#" class="cBtn expertChatGo jsGoExpertChat">채팅 참여하기</a>
                            </dd>
                        </dl>
                    </div>
                </div>
                <!-- // item -->

                <!-- item -->
                <div class="chatSliderItem">
                    <div class="inner">
                        <h3 class="message">
                            2코인 전문가에게 물어보세요!
                        </h3>
                        <p class="profile">
                            <span class="profileImg">
                                <img src="/chatbit_m/img/@temp-man.png" alt="">
                            </span>
                            <span class="profileText">
                                <span class="profileName ellipsis">코인전문가</span>
                                <span class="profileDesc ellipsis_multiple">
                                    투자가치가 있는 신규코인에 대한 정보를 나누고 싶습니다. 채팅/쪽지 환영입니다.
                                </span>
                            </span>
                        </p>
                        <dl class="roomInfo">
                            <dt class="roomName ellipsis">코인 시장 분석과 에어드랍 이벤트 안내</dt>
                            <dd class="roomCategory ellipsis">코인시세 > 이더리움클래식</dd>
                            <dd class="roomDesc">
                                <p class="ellipsis_multiple">투자 가치가 있는 신규코인에 대한 정보를 나누고 싶습니다. 새로운 코인에 관심있으신 분들 모두
                                    환영합니다.</p>
                                <a href="#" class="cBtn expertChatGo jsGoExpertChat">채팅 참여하기</a>
                            </dd>
                        </dl>
                    </div>
                </div>
                <!-- // item -->

                <!-- item -->
                <div class="chatSliderItem">
                    <div class="inner">
                        <h3 class="message">
                            3코인 전문가에게 물어보세요!
                        </h3>
                        <p class="profile">
                            <span class="profileImg">
                                <img src="/chatbit_m/img/@temp-man.png" alt="">
                            </span>
                            <span class="profileText">
                                <span class="profileName ellipsis">코인전문가</span>
                                <span class="profileDesc ellipsis_multiple">
                                    투자가치가 있는 신규코인에 대한 정보를 나누고 싶습니다. 채팅/쪽지 환영입니다.
                                </span>
                            </span>
                        </p>
                        <dl class="roomInfo">
                            <dt class="roomName ellipsis">코인 시장 분석과 에어드랍 이벤트 안내</dt>
                            <dd class="roomCategory ellipsis">코인시세 > 이더리움클래식</dd>
                            <dd class="roomDesc">
                                <p class="ellipsis_multiple">투자 가치가 있는 신규코인에 대한 정보를 나누고 싶습니다. 새로운 코인에 관심있으신 분들 모두
                                    환영합니다.</p>
                                <a href="#" class="cBtn expertChatGo jsGoExpertChat">채팅 참여하기</a>
                            </dd>
                        </dl>
                    </div>
                </div>
                <!-- // item -->

            </div>
            <div class="chatSliderDotsWrap"></div>
        </div>
        <!-- // .chatSliderWrap -->

        <div class="sectionTitle">
            <h3 class="text">일반채팅</h3>
            <button type="button" class="cBtn2 fill chatCreate jsCreateChat">채팅방 개설</button>
        </div>

        <div class="chatListFilter">
            <div class="chatCategory">
                <!-- 대분류 -->
                <div class="cateSelect gray">
                    <p class="cateSelectLabel">전체</p>
                    <select class="jsChatCateSelect">
                        <option value="1">전체</option>
                        <option value="2">ICO</option>
                        <option value="3">코인시세</option>
                        <option value="4">일반</option>
                    </select>
                </div>

                <!-- 중분류 -->
                <div class="cateSelect disabled">
                    <p class="cateSelectLabel">전체</p>
                    <select class="jsChatCateSelect" disabled>
                        <option value="1">전체</option>
                        <option value="1">신규코인</option>
                        <option value="1">백서토론</option>
                        <option value="2">비트코인</option>
                        <option value="3">이더리움</option>
                        <option value="4">리플</option>
                        <option value="4">비트코인캐시</option>
                        <option value="4">이더리움클래식</option>
                        <option value="4">이오스</option>
                        <option value="4">라이트코인</option>
                        <option value="4">퀀텀</option>
                        <option value="4">대시</option>
                        <option value="4">모네로</option>
                    </select>
                </div>
            </div>

            <div class="chatSort">
                <label class="orderCheckbox jsChatOrder">
                    <!-- input이 checked 되면 인기순 -->
                    <input type="checkbox" class="visuallyhidden" value="order">
                    <span class="orderCheckbox-text">
                        최신순
                    </span>
                </label>
            </div>
        </div>
        <!-- // .chatListFilter -->

        <div class="chatroomList">

            <!-- chat item -->
            <div class="chatroom">
                <a href="#">
                    <p class="cProfile">
                        <span class="cProfileImg">
                            <img src="/chatbit_m/img/@temp-man.png" alt="">
                        </span>
                        <span class="cProfileName ellipsis">김챗빗</span>
                        <span class="chatroomCategory ellipsis">
                            <span>코인시세</span>
                            <span>이더리움클래식</span>
                        </span>
                        <span class="cProfileTime">2018.7.23</span>
                    </p>
                    <h4 class="chatroomName ellipsis">
                        <span class="chatroomLabel open">공개</span>
                        8월 터질 코인들 대공개!
                    </h4>
                    <span class="chatroomNumber">53</span>
                    <p class="chatroomDesc ellipsis_multiple">
                        투자가치가 있는 신규코인에 대한 정보를 나누고 싶습니다. 새로운 코인에 관심있으신 분들 모두 환영합니다.
                    </p>
                </a>
            </div>
            <!-- // chat item -->

            <div class="chatroom jsPrivacyChat">
                <a href="#">
                    <p class="cProfile">
                        <span class="cProfileImg">
                            <img src="/chatbit_m/img/@temp-man.png" alt="">
                        </span>
                        <span class="cProfileName ellipsis">홍길동</span>
                        <span class="chatroomCategory ellipsis">
                            <span>코인시세</span>
                            <span>이더리움클래식</span>
                        </span>
                        <span class="cProfileTime">2018.7.23</span>
                    </p>
                    <h4 class="chatroomName ellipsis">
                        <span class="chatroomLabel">비공개</span>
                        수익을 챙기는 코인거래법!
                    </h4>
                    <span class="chatroomNumber">130</span>
                    <p class="chatroomDesc ellipsis_multiple">
                        투자가치가 있는 신규코인에 대한 정보를 나누고 싶습니다. 새로운 코인에 관심있으신 분들 모두 환영합니다.
                    </p>
                </a>
            </div>

            <div class="chatroom jsPrivacyChat">
                <a href="#">
                    <p class="cProfile">
                        <span class="cProfileImg">
                            <img src="/chatbit_m/img/@temp-man.png" alt="">
                        </span>
                        <span class="cProfileName ellipsis">홍길동</span>
                        <span class="chatroomCategory ellipsis">
                            <span>코인시세</span>
                            <span>이더리움클래식</span>
                        </span>
                        <span class="cProfileTime">2018.7.23</span>
                    </p>
                    <h4 class="chatroomName ellipsis">
                        <span class="chatroomLabel">비공개</span>
                        수익을 챙기는 코인거래법!
                    </h4>
                    <span class="chatroomNumber">53</span>
                    <p class="chatroomDesc ellipsis_multiple">
                        투자가치가 있는 신규코인에 대한 정보를 나누고 싶습니다. 새로운 코인에 관심있으신 분들 모두 환영합니다.
                    </p>
                </a>
            </div>

            <div class="chatroom">
                <a href="#">
                    <p class="cProfile">
                        <span class="cProfileImg">
                            <img src="/chatbit_m/img/@temp-man.png" alt="">
                        </span>
                        <span class="cProfileName ellipsis">김챗빗</span>
                        <span class="chatroomCategory ellipsis">
                            <span>코인시세</span>
                            <span>이더리움클래식</span>
                        </span>
                        <span class="cProfileTime">2018.7.23</span>
                    </p>
                    <h4 class="chatroomName ellipsis">
                        <span class="chatroomLabel open">공개</span>
                        수익을 챙기는 코인거래법!
                    </h4>
                    <span class="chatroomNumber">53</span>
                    <p class="chatroomDesc ellipsis_multiple">
                        투자가치가 있는 신규코인에 대한 정보를 나누고 싶습니다. 새로운 코인에 관심있으신 분들 모두 환영합니다.
                    </p>
                </a>
            </div>



        </div>
        <!-- // .chatroomList -->

        <button type="button" class="articleMore">더보기</button>

    </div>
    <!-- // .containerWrap -->

    <!-- 비밀번호 입력 팝업 -->
    <div class="cPopup-mini popupChatPw">
        <div class="cPopupInner-mini">
            <h3 class="cPopupTitle-mini">채팅방 비밀번호</h3>
            <button type="button" class="cPopupClose-mini jsChatPwClose" aria-label="Close this popup">
                <span class="btnX small"></span>
            </button>
            <form action="#">
                <fieldset>
                    <p class="inputField">
                        <label for="privacyChatPw" class="visuallyhidden">비공개 채팅방 비밀번호를 입력해 주세요.</label>
                        <span class="input blueLine">
                            <input type="password" id="privacyChatPw" placeholder="비밀번호를 입력해 주세요." value="">
                        </span>
                        <strong class="message">Validation message</strong>
                    </p>
                    <div class="cPopupFooter-mini rowGroup">
                        <button type="button" class="cBtn line jsChatPwClose">취소</button>
                        <button class="cBtn">확인</button>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
    <!-- // 비밀번호 입력 팝업 -->

    <!-- 채팅방 개설 팝업 -->
    <div class="cPopup popupCreateChat">
        <div class="cPopupInner">
            <h2 class="cPopupTitle">채팅방 개설</h2>
            <button type="button" class="cPopupClose jsPopupClose" aria-label="Close this popup">
                <span class="btnX"></span>
            </button>

            <div class="cPopupBody typeCreateChat">
                <div class="createChatCategory">
                    <!-- 대분류 -->
                    <div class="cateSelect2">
                        <p class="cateSelect2Label">코인시세</p>
                        <select class="jsChatCateSelect">
                            <option value="1">전체</option>
                            <option value="2">ICO</option>
                            <option value="3" selected>코인시세</option>
                            <option value="4">일반</option>
                        </select>
                    </div>

                    <!-- 중분류 -->
                    <div class="cateSelect2">
                        <p class="cateSelect2Label">이더리움클래식</p>
                        <select class="jsChatCateSelect">
                            <option value="1">전체</option>
                            <option value="1">신규코인</option>
                            <option value="1">백서토론</option>
                            <option value="2">비트코인</option>
                            <option value="3">이더리움</option>
                            <option value="4">리플</option>
                            <option value="4">비트코인캐시</option>
                            <option value="4" selected>이더리움클래식</option>
                            <option value="4">이오스</option>
                            <option value="4">라이트코인</option>
                            <option value="4">퀀텀</option>
                            <option value="4">대시</option>
                            <option value="4">모네로</option>
                        </select>
                    </div>
                </div>

                <div class="writeCategory">
                    <label class="cRadio2">
                        <input type="radio" class="visuallyhidden" name="roomType" value="open" checked="">
                        <span class="cRadio2-text">
                            공개
                        </span>
                    </label>
                    <label class="cRadio2">
                        <input type="radio" class="visuallyhidden" name="roomType" value="notOpen">
                        <span class="cRadio2-text">
                            비공개
                        </span>
                    </label>
                </div>

                <p class="inputField writeTitle">
                    <label for="chatTitle" class="visuallyhidden">채팅방 이름 입력해 주세요.</label>
                    <span class="input">
                        <input type="text" id="chatTitle" value="" placeholder="제목을 입력해 주세요.">
                    </span>
                    <strong class="message">사용할 수 없습니다.</strong>
                </p>

                <div class="writeBodyWrap jsEditor">
                    <div class="writeBody">
                        <textarea class="editor"></textarea>
                    </div>
                    <div class="editorPlaceholder">
                        <p>채팅방에 대한 설명을 입력해 주세요.<br />(최대 300자)</p>
                    </div>
                </div>
                <div class="writeBodyMeta">
                    <span class="writeEditorLength">0/300</span>
                </div>

                <div class="writeButtons rowGroup">
                    <button type="button" class="cBtn line">취소</button>
                    <button class="cBtn">등록</button>
                </div>
            </div>
        </div>
    </div>
    <!-- // 채팅방 개설 팝업 -->

    <hr class="h9">

    <jsp:include page="/chatbit_m/include/chatApp.jsp" flush="true"></jsp:include>
    <script>
        // 채팅창 열기
        $('.chatroomList').on('click', '.chatroom', function () {
            if (!$(this).hasClass('jsPrivacyChat')) {
                $('.jsChatApp').fadeIn();
            }
        });
    </script>

    <jsp:include page="/chatbit_m/include/footer.jsp" flush="true"></jsp:include>

    <script>
        // 체크박스가 선택되면 인기순으로 변경
        $('.jsChatOrder').click(function () {
            var $this = $(this);
            var $input = $this.find('input');
            var $text = $this.find('.orderCheckbox-text');

            if ($input.prop('checked')) {
                $text.text('인기순');
            } else {
                $text.text('최신순');
            }
        });

        // 카테고리 셀렉트박스 변경될 때 텍스트 변경
        $('.jsChatCateSelect').each(function (i, selectBox) {
            $(selectBox).change(function () {
                var $this = $(this);
                $this.prev().text($this.find('option:selected').text());
            });
        });

        $('.jsPrivacyChat').click(function (e) {
            e.preventDefault();
            dimOn();
            $('.popupChatPw').fadeIn();
        });

        $('.jsChatPwClose').click(function () {
            dimOff();
            $(this).parents('.cPopup-mini').fadeOut(200);
        });

        $('.jsGoExpertChat').click(function () {
            confirm('전문가의 채팅방 입장 시 20포인트가 차감됩니다. 입장하시겠습니까?');
        });

        $('.jsCreateChat').click(function () {
            var check = confirm('채팅방 개설 시 10포인트가 차감됩니다. 채팅방을 개설하시겠습니까?');
            if (check) {
                $('.popupCreateChat').fadeIn(200);
            }
        });

        $(document).ready(function () {
            $('.jsChatSlider').slick({
                autoplay: true,
                // autoplaySpeed: 2000,
                dots: true,
                arrows: false,
                infinite: true,
                speed: 100,
                fade: true,
                cssEase: "linear",
                appendDots: $('.chatSliderDotsWrap'),
                zIndex: 10,
            });
        });
    </script>

</body>

</html>