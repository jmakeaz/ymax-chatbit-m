<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" %>
<html lang="ko">

<head>
    <title> CHATBIT - YMAX </title>
    <meta charset="utf-8" />
    <meta name="format-detection" content="telephone=no" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <link href="/chatbit_m/css/base.css" type="text/css" rel="stylesheet" />
    <link href="/chatbit_m/css/layout.css" type="text/css" rel="stylesheet" />
    <link href="/chatbit_m/css/common.css" type="text/css" rel="stylesheet" />
    <link href="/chatbit_m/css/member.css" type="text/css" rel="stylesheet" />
    <script src="/chatbit/js/1.10.1.jquery.min.js" type="text/javascript" charset="utf-8"></script>
</head>

<body>

    <div class="containerWrap">
        <div class="headerMini">
            <h1 class="logo">
                <a href="/chatbit_m/">
                    <img src="/chatbit_m/img/svg/bi.svg" alt="Chatbit">
                </a>
            </h1>
        </div>

        <div class="signupPage">
            <h2 class="visuallyhidden">회원가입</h2>
            <p class="signupRecommend">SNS 아이디를 이용해서<br /> <span class="colorPrimary">Chatbit</span> 회원으로 가입합니다.</p>
            <ul class="loginList">
                <li><a href="./signup2.jsp" class="btnFb"><span class="iconFb"></span> 페이스북으로 회원가입</a></li>
                <li><a href="./signup2.jsp" class="btnNaver"><span class="iconNaver"></span>네이버로 회원가입</a></li>
                <li><a href="./signup2.jsp" class="btnKakao"><span class="iconKakao"></span>카카오계정으로 회원가입</a></li>
            </ul>
        </div>
    </div>

    <jsp:include page="/chatbit_m/include/footer.jsp" flush="true"></jsp:include>

</body>

</html>