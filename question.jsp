<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" %>
<html lang="ko">

<head>
    <title> CHATBIT - YMAX </title>
    <meta charset="utf-8" />
    <meta name="format-detection" content="telephone=no" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <link href="/chatbit_m/css/base.css" type="text/css" rel="stylesheet" />
    <link href="/chatbit_m/css/layout.css" type="text/css" rel="stylesheet" />
    <link href="/chatbit_m/css/common.css" type="text/css" rel="stylesheet" />
    <link href="/chatbit_m/css/board.css" type="text/css" rel="stylesheet" />
    <script src="/chatbit/js/1.10.1.jquery.min.js" type="text/javascript" charset="utf-8"></script>
</head>

<body>

    <jsp:include page="/chatbit_m/include/header.jsp" flush="true"></jsp:include>

    <div class="containerWrap">

        <jsp:include page="/chatbit_m/include/customerMenu.jsp" flush="true"></jsp:include>
        <script>
            $('.jsBoardLinks [data-value="2"]').trigger('click');
        </script>

        <p class="questionNotice">
            <strong>1:1 문의하기</strong> 내역과 답변을 확인할 수 있습니다.
            <br> 문의 내용에 개인정보가 포함되거나 중복 문의인 경우 관리자에 의해 삭제될 수 있습니다.<br />
            <a href="/chatbit_m/questionWrite.jsp" class="cBtn2 fill"> Contact us 1:1</a>
        </p>

        <!-- article list -->
        <div class="questionBoard">
            <ul class="questionList">
                <li class="wait">
                    <a href="./questionView.jsp" class="questionItem">
                        <div class="questionItemHeader">
                            <p class="questionItemTitle ellipsis">문의드립니다.</p>
                            <span class="questionItemTime">2018.08.09</span>
                        </div>
                        <strong class="questionItemResult">답변대기</strong>
                    </a>
                </li>
                <li class="done">
                    <a href="./questionView.jsp" class="questionItem">
                        <div class="questionItemHeader">
                            <p class="questionItemTitle ellipsis">게시글도 올리고 댓글도 달고 이것저것</p>
                            <span class="questionItemTime">2018.08.09</span>
                        </div>
                        <strong class="questionItemResult">답변완료</strong>
                    </a>
                </li>
                <li class="done">
                    <a href="./questionView.jsp" class="questionItem">
                        <div class="questionItemHeader">
                            <p class="questionItemTitle ellipsis">게임 중에 에러가 났는데 어또케?</p>
                            <span class="questionItemTime">2018.08.09</span>
                        </div>
                        <strong class="questionItemResult">답변완료</strong>
                    </a>
                </li>
            </ul>
        </div>
        <!-- article list -->
        <button type="button" class="articleMore">더보기</button>
    </div>

    <hr class="h9">

    <jsp:include page="/chatbit_m/include/footer.jsp" flush="true"></jsp:include>

</body>

</html>