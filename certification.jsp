<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" %>
<html lang="ko">

<head>
    <title> CHATBIT - YMAX </title>
    <meta charset="utf-8" />
    <meta name="format-detection" content="telephone=no" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <link href="/chatbit_m/css/base.css" type="text/css" rel="stylesheet" />
    <link href="/chatbit_m/css/layout.css" type="text/css" rel="stylesheet" />
    <link href="/chatbit_m/css/common.css" type="text/css" rel="stylesheet" />
    <link href="/chatbit_m/css/board.css" type="text/css" rel="stylesheet" />
    <script src="/chatbit/js/1.10.1.jquery.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="/chatbit_m/js/jquery.dotdotdot.js" type="text/javascript" charset="utf-8"></script>
</head>

<body>

    <jsp:include page="/chatbit_m/include/header.jsp" flush="true"></jsp:include>

    <div class="containerWrap">

        <jsp:include page="/chatbit_m/include/boardMenu.jsp" flush="true"></jsp:include>
        <script>
            $('.jsBoardLinks [data-value="2"]').trigger('click');
        </script>

        <!-- sort -->
        <div class="sortingGroup">
            <label class="orderCheckbox jsChatOrder">
                <!-- input이 checked 되면 인기순 -->
                <input type="checkbox" class="visuallyhidden" value="order">
                <span class="orderCheckbox-text">최신순</span>
            </label>
        </div>
        <!-- // sort -->

        <!-- article list -->
        <div class="certificationList">
            <div class="articleCard plus">
                <a href="/chatbit_m/certificationView.jsp">
                    <p class="cProfile">
                        <span class="cProfileImg">
                            <!-- <img src="https://placebeyonce.com/640-360" alt=""> -->
                        </span>
                        <span class="cProfileName ellipsis">김챗빗</span>
                        <span class="cProfileTime">2018.7.23</span>
                    </p>
                    <p class="articleCardTitle ellipsis_multiple">
                        챗빗이의 첫 수익을 인증합니다.
                        챗빗이의 첫 수익을 인증합니다.
                        챗빗이의 첫 수익을 인증합니다.
                    </p>
                    <div class="articleCardImg">
                        <!-- <img src="/chatbit_m/img/@temp1.png" alt=""> -->
                        <span class="newsLabel">수익</span>
                    </div>
                    <p class="articleCardInfo">
                        <span class="cLikes">45</span>
                        <span class="cComments">99+</span>
                    </p>
                </a>
            </div>
            <div class="articleCard plus">
                <a href="/chatbit_m/certificationView.jsp">
                    <p class="cProfile">
                        <span class="cProfileImg">
                            <img src="https://placebeyonce.com/640-360" alt="">
                        </span>
                        <span class="cProfileName ellipsis">김챗빗</span>
                        <span class="cProfileTime">2018.7.23</span>
                    </p>
                    <p class="articleCardTitle ellipsis_multiple">
                        챗빗이의 첫 수익을 인증합니다.
                        챗빗이의 첫 수익을 인증합니다.
                        챗빗이의 첫 수익을 인증합니다.
                    </p>
                    <div class="articleCardImg">
                        <img src="/chatbit_m/img/@temp1.png" alt="">
                        <span class="newsLabel">수익</span>
                    </div>
                    <p class="articleCardInfo">
                        <span class="cLikes">45</span>
                        <span class="cComments">99+</span>
                    </p>
                </a>
            </div>

            <div class="articleCard minus">
                <a href="/chatbit_m/certificationView.jsp">
                    <p class="cProfile">
                        <span class="cProfileImg">
                            <img src="https://placebeyonce.com/640-360" alt="">
                        </span>
                        <span class="cProfileName ellipsis" title="홍길동코인">김챗빗</span>
                        <span class="cProfileTime">2018.6.18</span>
                    </p>
                    <p class="articleCardTitle ellipsis_multiple">
                        챗빗이의 첫 손실을 인증합니다.
                    </p>
                    <div class="articleCardImg">
                        <img src="/chatbit_m/img/@temp2.png" alt="">
                        <span class="newsLabel">손실</span>
                    </div>
                    <p class="articleCardInfo">
                        <span class="cLikes">1234</span>
                        <span class="cComments">45</span>
                    </p>
                </a>
            </div>

            <div class="articleCard plus blocked">
                <a href="/chatbit_m/certificationView.jsp">
                    <p class="cProfile">
                        <span class="cProfileImg">
                            <img src="https://placebeyonce.com/640-360" alt="">
                        </span>
                        <span class="cProfileName ellipsis">홍길동코인 홍길동코인 홍길동코인</span>
                        <span class="cProfileTime">2018.6.18</span>
                    </p>
                    <p class="articleCardTitle ellipsis_multiple">
                        챗빗이의 두번째 수익을 인증합니다.
                    </p>
                    <div class="articleCardImg">
                        <img src="/chatbit_m/img/@temp1.png" alt="">
                    </div>
                    <p class="articleCardInfo">
                        <span class="cLikes">1234</span>
                        <span class="cComments">45</span>
                    </p>
                </a>
            </div>
        </div>
        <!-- article list -->
        <button type="button" class="articleMore">더보기</button>
    </div>

    <hr class="h9">

    <jsp:include page="/chatbit_m/include/footer.jsp" flush="true"></jsp:include>
    <script>
        // 체크박스가 선택되면 인기순으로 변경
        $('.jsChatOrder').click(function () {
            var $this = $(this);
            var $input = $this.find('input');
            var $text = $this.find('.orderCheckbox-text');

            if ($input.prop('checked')) {
                $text.text('인기순');
            } else {
                $text.text('최신순');
            }
        });
    </script>

</body>

</html>