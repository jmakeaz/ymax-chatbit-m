<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" %>
<html lang="ko">

<head>
    <title> CHATBIT - YMAX </title>
    <meta charset="utf-8" />
    <meta name="format-detection" content="telephone=no" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <link href="/chatbit_m/css/base.css" type="text/css" rel="stylesheet" />
    <link href="/chatbit_m/css/layout.css" type="text/css" rel="stylesheet" />
    <link href="/chatbit_m/css/common.css" type="text/css" rel="stylesheet" />
    <link href="/chatbit_m/css/member.css" type="text/css" rel="stylesheet" />
    <script src="/chatbit/js/1.10.1.jquery.min.js" type="text/javascript" charset="utf-8"></script>
</head>

<body>

    <div class="containerWrap">
        <div class="emailDonePage">
            <h1 class="title">
                이메일 인증이<br />
                완료 되었습니다.
            </h1>
            <p class="desc">
                Chatbit의 다양한 서비스를<br />
                이용해 보세요.
            </p>
            <a href="/chatbit_m/" class="cBtn">메인으로 이동</a>
        </div>
    </div>

    <jsp:include page="/chatbit_m/include/footer.jsp" flush="true"></jsp:include>

</body>

</html>