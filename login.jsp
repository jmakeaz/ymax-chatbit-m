<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" %>
<html lang="ko">

<head>
    <title> CHATBIT - YMAX </title>
    <meta charset="utf-8" />
    <meta name="format-detection" content="telephone=no" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <link href="/chatbit_m/css/base.css" type="text/css" rel="stylesheet" />
    <link href="/chatbit_m/css/layout.css" type="text/css" rel="stylesheet" />
    <link href="/chatbit_m/css/common.css" type="text/css" rel="stylesheet" />
    <link href="/chatbit_m/css/member.css" type="text/css" rel="stylesheet" />
    <script src="/chatbit/js/1.10.1.jquery.min.js" type="text/javascript" charset="utf-8"></script>
</head>

<body>

    <div class="containerWrap">
        <div class="headerMini">
            <h1 class="logo">
                <a href="/chatbit_m/">
                    <img src="/chatbit_m/img/svg/bi.svg" alt="Chatbit">
                </a>
            </h1>
        </div>

        <div class="loginPage">
            <h2 class="visuallyhidden">로그인</h2>
            <ul class="loginList">
                <li><a href="#" class="btnFb"><span class="iconFb"></span> 페이스북으로 로그인</a></li>
                <li><a href="#" class="btnNaver"><span class="iconNaver"></span>네이버로 로그인</a></li>
                <li><a href="#" class="btnKakao"><span class="iconKakao"></span>카카오계정으로 로그인</a></li>
            </ul>
            <form action="#" class="loginRemember">
                <span class="cCheckbox">
                    <input id="login_keep" class="visuallyhidden" type="checkbox">
                    <label for="login_keep" class="cCheckboxLabel">로그인 상태 유지</label>
                </span>
                <button type="button" class="loginSearchId jsSearchId">계정찾기</button>
            </form>
            <p class="loginRecommend">
                <span>회원이 아니신가요?</span> <a href="/chatbit_m/signup.jsp" class="colorPrimary">회원가입하기</a>
            </p>
        </div>
    </div>

    <button class="jsSleepId">휴면계정 팝업 열기</button>

    <!-- 계정찾기 레이어 팝업 -->
    <div class="cPopup popupSearchId">
        <div class="cPopupInner">
            <h2 class="cPopupTitle">계정찾기</h2>
            <button type="button" class="cPopupClose jsPopupClose" aria-label="Close this popup">
                <span class="btnX"></span>
            </button>
            <p class="cPopupDesc">
                연동하신 SNS 계정을 잊으셨나요?<br>
                Chatbit 회원가입 시 등록한 이메일 주소를 입력해 주세요.
            </p>
            <form action="#">
                <p class="inputField">
                    <label for="confirmEmail" class="visuallyhidden">이메일 주소</label>
                    <span class="input">
                        <input type="email" id="confirmEmail" placeholder="이메일 주소 입력" value="">
                    </span>
                    <strong class="message">사용할 수 없습니다.</strong>
                </p>
                <button class="cBtn" onclick="alert('계정찾기 메일을 발송했습니다.');">보내기</button>
            </form>
            <p class="searchIdNotice">
                계정찾기로도 계정을 찾을 수 없을 경우<br> 고객센터 <a href="mailto:help@ymax.co.kr">help@ymax.co.kr</a>로 문의해 주세요.
            </p>
        </div>
    </div>
    <!-- // 계정찾기 레이어 팝업 -->

    <!-- 휴면계정 해제 레이어 팝업 -->
    <div class="cPopup popupSleepId">
        <div class="cPopupInner">
            <h2 class="cPopupTitle">휴면계정 해제</h2>
            <button type="button" class="cPopupClose jsPopupClose" aria-label="Close this popup">
                <span class="btnX"></span>
            </button>
            <p class="cPopupDescBold">Chatbit 계정의 휴면을 해제해 주세요.</p>
            <p class="cPopupDesc">
                회원님의 계정을 안전하게 보호하기 위해
                1년 이상 로그인하지 않은 경우 휴면 상태로 전환됩니다.
                서비스를 계속 이용하시려면 이메일 인증을 받으신 후
                다시 로그인 해주세요.
            </p>
            <form action="#">
                <p class="inputField">
                    <label for="confirmEmail2" class="visuallyhidden">이메일 주소</label>
                    <span class="input">
                        <input type="email"" id=" confirmEmail2" placeholder="이메일 주소 입력" value="">
                    </span>
                    <strong class="message">사용할 수 없습니다.</strong>
                </p>
                <button class="cBtn" onclick="alert('인증메일을 전송했습니다.');">인증메일 전송</button>
            </form>
            <p class="sleepIdNotice">
                인증메일을 받지 못하셨다면 <button class="colorPrimary" onclick="alert('인증메일을 재전송했습니다.');">인증메일 재전송</button>을
                눌러주세요. 메일에 따라 도착 시간이 조금 지연될 수
                있습니다. 오랫동안 오지 않을 경우 스팸메일함을
                확인해 주세요.
            </p>
        </div>
    </div>
    <!-- // 휴면계정 해제 레이어 팝업 -->

    <jsp:include page="/chatbit_m/include/footer.jsp" flush="true"></jsp:include>

    <script>
        $('.jsPopupClose').click(function () {
            $(this).parents('.cPopup').fadeOut(200);
        });

        $('.jsSearchId').click(function () {
            $('.popupSearchId').fadeIn(200);
        });

        $('.jsSleepId').click(function () {
            $('.popupSleepId').fadeIn(200);
        });
    </script>

</body>

</html>