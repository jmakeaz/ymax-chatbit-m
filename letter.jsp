<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" %>
<html lang="ko">

<head>
    <title> CHATBIT - YMAX </title>
    <meta charset="utf-8" />
    <meta name="format-detection" content="telephone=no" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <link href="/chatbit_m/css/base.css" type="text/css" rel="stylesheet" />
    <link href="/chatbit_m/css/layout.css" type="text/css" rel="stylesheet" />
    <link href="/chatbit_m/css/common.css" type="text/css" rel="stylesheet" />
    <link href="/chatbit_m/css/letter.css" type="text/css" rel="stylesheet" />
    <script src="/chatbit/js/1.10.1.jquery.min.js" type="text/javascript" charset="utf-8"></script>
</head>

<body>

    <jsp:include page="/chatbit_m/include/header.jsp" flush="true"></jsp:include>

    <div class="containerWrap">

        <h2 class="pageTitle">쪽지</h2>

        <ul class="letterList">
            <li>
                <a href="#" class="letterItem jsLetterItem">
                    <span class="cProfileImg size45 letterImg">
                        <!-- <img src="https://www.fillmurray.com/640/360" alt=""> -->
                    </span>
                    <strong class="letterRecipient ellipsis">자산관리사</strong>
                    <span class="letterUnread" aria-label="Number of unread letters">99+</span>
                    <span class="letterTime">오후 11:47</span>
                    <p class="letterText ellipsis">마지막 메시지 내용 일부를 표시합니다. 마지막 메시지 내용 일부를 표시합니다.</p>
                </a>
            </li>
            <li>
                <a href="#" class="letterItem jsLetterItem">
                    <span class="cProfileImg size45 letterImg">
                        <img src="https://www.fillmurray.com/640/360" alt="">
                    </span>
                    <strong class="letterRecipient ellipsis">리사</strong>
                    <span class="letterUnread" aria-label="Number of unread letters">25</span>
                    <span class="letterTime">오후 11:47</span>
                    <p class="letterText ellipsis">^^;</p>
                </a>
            </li>
            <li>
                <a href="#" class="letterItem jsLetterItem">
                    <span class="cProfileImg size45 letterImg">
                        <img src="https://www.fillmurray.com/640/360" alt="">
                    </span>
                    <strong class="letterRecipient ellipsis">뿡뿡이</strong>
                    <!-- <span class="letterUnread" aria-label="Number of unread letters">1</span> -->
                    <span class="letterTime">오후 11:47</span>
                    <p class="letterText ellipsis">마지막 메시지 내용</p>
                </a>
            </li>
            <li>
                <a href="#" class="letterItem jsLetterItem">
                    <span class="cProfileImg size45 letterImg">
                        <img src="https://www.fillmurray.com/640/360" alt="">
                    </span>
                    <strong class="letterRecipient ellipsis">타짜</strong>
                    <!-- <span class="letterUnread" aria-label="Number of unread letters">1</span> -->
                    <span class="letterTime">11월 14일</span>
                    <p class="letterText ellipsis">포인트 메시지 내용이 일일 부부</p>
                </a>
            </li>
            <li>
                <a href="#" class="letterItem jsLetterItem">
                    <span class="cProfileImg size45 letterImg">
                        <img src="https://www.fillmurray.com/640/360" alt="">
                    </span>
                    <strong class="letterRecipient ellipsis">홍길동</strong>
                    <!-- <span class="letterUnread" aria-label="Number of unread letters">1</span> -->
                    <span class="letterTime">2017. 12. 25</span>
                    <p class="letterText ellipsis">마지막 메시지 내용 일부가</p>
                </a>
            </li>
        </ul>
        <!-- <button type="button" class="articleMore">더보기</button> -->
    </div>
    <hr class="h9" />

    <div class="cPopup letterPopup">
        <div class="letterHeader">
            <p class="cProfile">
                <span class="cProfileImg">
                    <img src="https://placebeyonce.com/640-360" alt="">
                </span>
                <span class="cProfileName ellipsis">리사</span>
            </p>
            <div class="letterHeaderMore">
                <button type="button" class="btnMoreDots jsLetterMore">
                    <span class="dot"></span>
                    <span class="dot"></span>
                    <span class="dot"></span>
                    <span class="visuallyhidden">쪽지 설정</span>
                </button>
                <div class="fn">
                    <button type="button" class="jsDeleteLetter">삭제</button>
                    <button type="button" class="jsBlockLetter">대상차단</button>
                </div>
            </div>
            <button type="button" class="cPopupClose jsPopupClose" aria-label="Close this popup">
                <span class="btnX"></span>
            </button>
        </div>
        <!-- // .letterHeader -->

        <div class="letterBody">
            <div class="jsLetterContents">
                <div class="letterBox">
                    <span class="cProfileImg size32 letterBoxImg">
                        <!-- <img src="https://www.fillmurray.com/640/360" alt=""> -->
                    </span>
                    <strong class="letterBoxName">누구냐 넌?</strong>
                    <span class="letterBoxTime">오전 4:25</span>
                    <p class="letterBoxText">
                        포인트 변환을 하다가 에러가 났는데 제 포인트는 줄었는데 게임 포인트는 추가가 안됐거든요…….. 제 포인트……………
                        확인 좀 부탁드려요 .
                    </p>
                </div>
                <div class="letterBox">
                    <span class="cProfileImg size32 letterBoxImg">
                        <img src="https://placebeyonce.com/640-360" alt="">
                    </span>
                    <strong class="letterBoxName">리사</strong>
                    <span class="letterBoxTime">오전 4:25</span>
                    <p class="letterBoxText">
                        이봐요! 왜 사람이 살려 달라고 소리치는 데도 구해주지 않았소?
                    </p>
                </div>

                <p class="letterTimeLine"><span class="value">2018년 7월 3일 화요일</span></p>

                <div class="letterBox">
                    <span class="cProfileImg size32 letterBoxImg">
                        <img src="https://placebeyonce.com/640-360" alt="">
                    </span>
                    <strong class="letterBoxName">리사</strong>
                    <span class="letterBoxTime">오전 4:25</span>
                    <p class="letterBoxText">
                        왜? 어째서? 응? 뿡뿡뿡~~ 꺄~
                    </p>
                </div>
                <div class="letterBox unread">
                    <span class="cProfileImg size32 letterBoxImg">
                        <!-- <img src="https://www.fillmurray.com/640/360" alt=""> -->
                    </span>
                    <strong class="letterBoxName">누구냐 넌?</strong>
                    <span class="letterBoxTime">오전 4:25</span>
                    <p class="letterBoxText">
                        ^^;
                    </p>
                </div>
            </div>
        </div>

        <div class="letterFooter">
            <textarea class="jsLetterEditor" placeholder="쪽지 내용을 입력해 주세요."></textarea>
            <button type="button" class="btnLetterSend">
                <span class="iconPlane"></span>
                <span class="visuallyhidden">보내기</span>
            </button>
        </div>

    </div>

    <jsp:include page="/chatbit_m/include/footer.jsp" flush="true"></jsp:include>

    <script>
        $(document).ready(function () {

            // 쪽지 textarea 높이 
            var $letterEditor = $('.jsLetterEditor');
            $letterEditor.focus(function () {
                $letterEditor.parents('.letterPopup').addClass('writing');
            });
            $letterEditor.blur(function () {
                $letterEditor.parents('.letterPopup').removeClass('writing');
            });
        });

        // 쪽지 삭제, 차단 레이어 토글
        $('.jsLetterMore').click(function (e) {
            e.stopPropagation();
            $(this).toggleClass('active');
        });

        // 쪽지 상세 팝업 열기
        $('.jsLetterItem').click(function (e) {
            e.preventDefault();
            $('.letterPopup').fadeIn();
            $('.letterBody').scrollTop($('.jsLetterContents').outerHeight());
        });

        // 쪽지 삭제
        $('.jsDeleteLetter').click(function () {
            confirm('주고받은 모든 쪽지가 삭제됩니다. 삭제하시겠습니까?');
        });

        // 대상 차단
        $('.jsBlockLetter').click(function () {
            confirm('홍길동 님과 더 이상 쪽지를 주고받으실 수 없습니다. 차단하시겠습니까?');
        });

        $('body').on('touchstart click', function (e) {

            // 쪽지 삭제, 차단 레이어 닫기
            if ($('.jsLetterMore').hasClass('active')) {
                if ($('.letterHeaderMore').has(e.target).length === 0) {
                    $('.jsLetterMore').removeClass('active');
                }
            }
        });
    </script>

</body>

</html>