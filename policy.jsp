<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" %>
<html lang="ko">

<head>
    <title> CHATBIT - YMAX </title>
    <meta charset="utf-8" />
    <meta name="format-detection" content="telephone=no" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <link href="/chatbit_m/css/base.css" type="text/css" rel="stylesheet" />
    <link href="/chatbit_m/css/layout.css" type="text/css" rel="stylesheet" />
    <link href="/chatbit_m/css/common.css" type="text/css" rel="stylesheet" />
    <link href="/chatbit_m/css/customer.css" type="text/css" rel="stylesheet" />
    <script src="/chatbit/js/1.10.1.jquery.min.js" type="text/javascript" charset="utf-8"></script>
</head>

<body>

    <jsp:include page="/chatbit_m/include/header.jsp" flush="true"></jsp:include>

    <div class="containerWrap">

        <jsp:include page="/chatbit_m/include/customerMenu.jsp" flush="true"></jsp:include>
        <script>
            $('.jsBoardLinks [data-value="3"]').trigger('click');
        </script>

        <div class="contentPolicy">
            <p style="color: #222;">1. 이용권한</p>
            <p>모든 게시판은 운영진의 별도의 권한 설정이 없는 한 회원들 누구나 읽기 가능한 게시판 입니다. 단, 비회원의 경우 댓글을 최대 3개까지만 읽을 수 있습니다.</p>
            <p style="color: #222; margin-top: 20px;">2. 글에 대한 책임</p>
            <p>모든 게시물은 작성자 본인이 책임을 져야 하며 이와 관련하여 민형사상 책임이 발생할 경우 챗비트 및 관리자(이하 '챗비트')는 책임을 지지 않습니다.</p>
            <p style="color: #222; margin-top: 20px;">3. 회원 제재</p>
            <p>모든 게시판은 아래와 같은 사항의 글이 있을 경우 해당 게시물 작성자에게 통보 없이 운영진에서 삭제 및 이용제한(1차 관리자 경고, 2차 기간정지(7일), 3차 영구정지)을 가할 수
                있습니다.
                또한 2차 제재를 3회 이상 받을 시 자동으로 3차 제재(영구정지)가 적용됩니다</p>
            <p>3-1. 개인정보 이용제한(3차 제제)</p>
            <p style="margin-left: 8px;">- 타인의 개인정보도용 및 유포</p>
        </div>
    </div>

    <jsp:include page="/chatbit_m/include/footer.jsp" flush="true"></jsp:include>

</body>

</html>