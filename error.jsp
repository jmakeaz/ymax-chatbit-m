<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" %>
<html lang="ko">

<head>
    <title> CHATBIT - YMAX </title>
    <meta charset="utf-8" />
    <meta name="format-detection" content="telephone=no" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <link href="/chatbit_m/css/base.css" type="text/css" rel="stylesheet" />
    <link href="/chatbit_m/css/layout.css" type="text/css" rel="stylesheet" />
    <link href="/chatbit_m/css/common.css" type="text/css" rel="stylesheet" />
    <script src="/chatbit/js/1.10.1.jquery.min.js" type="text/javascript" charset="utf-8"></script>
</head>

<body>

    <jsp:include page="/chatbit_m/include/header.jsp" flush="true"></jsp:include>

    <div class="containerWrap">

        <div class="errorPage">
            <h2 class="errorPageTitle">서버 점검 중입니다.</h2>
            <p class="errorPageDesc">
                안녕하세요. <strong>Chatbit</strong> 입니다.<br />
                사이트 안정화 및 더 나은 서비스를 제공하고자
                서버 점검 작업을 실시하고 있습니다.<br />
                <strong>서버 점검은 오전 00:00부터 04:30까지 진행될 예정</strong>
                이며 작업 시간 동안 접속이 제한됩니다.<br />
                빠른 시간 내에 서비스가 정상화 되도록
                노력하겠습니다. <br />
                <span class="thanks">감사합니다.</span>
            </p>
        </div>

    </div>

    <jsp:include page="/chatbit_m/include/footer.jsp" flush="true"></jsp:include>

</body>

</html>