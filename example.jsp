<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" %>
<html lang="ko">

<head>
    <title> CHATBIT - YMAX </title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="/chatbit_m/css/base.css" type="text/css" rel="stylesheet" />
    <link href="/chatbit_m/css/layout.css" type="text/css" rel="stylesheet" />
    <link href="/chatbit_m/css/common.css" type="text/css" rel="stylesheet" />
    <script src="/chatbit/js/1.10.1.jquery.min.js" type="text/javascript" charset="utf-8"></script>
</head>

<body>

    <jsp:include page="/chatbit_m/include/header.jsp" flush="true"></jsp:include>

    <div class="containerWrap">
        <div style="margin: 40px 5px;">

            <!-- Icon -->
            <h2 style="margin-bottom: 20px; font-size: 24px; border-bottom: 1px solid #83dbe0; line-height: 2;">Icon</h2>
            <div style="padding: 1em; background-color: #000;">
                <span class="iconFb"></span>
                <span style="padding-left: 20px;"></span>
                <span class="iconNaver"></span>
                <span style="padding-left: 20px;"></span>
                <span class="iconKakao"></span>
                <span style="padding-left: 20px;"></span>
                <span class="iconCamera"></span>
            </div>
            <div style="padding: 1em;">
                <span class="iconShare"></span>
                <span style="padding-left: 20px;"></span>
                <span class="iconReport"></span>
                <span style="padding-left: 20px;"></span>
                <span class="iconBookmark"></span>
                <span style="padding-left: 20px;"></span>
                <span class="iconSetting"></span>
                <span style="padding-left: 20px;"></span>
                <span class="iconLetter"></span>
                <span style="padding-left: 20px;"></span>
                <span class="cComments">45</span>
                <span style="padding-left: 20px;"></span>
                <span class="cLikes">1234</span>
                <span style="padding-left: 20px;"></span>
                <span class="iconPost"></span>
                <span style="padding-left: 20px;"></span>
                <span class="iconNotice"></span>
                <span style="padding-left: 20px;"></span>
                <span class="iconPoint"></span>
                <span style="padding-left: 20px;"></span>
                <span class="iconExchange"></span>
                <span style="padding-left: 20px;"></span>
                <span class="iconPhoto"></span>
                <span style="padding-left: 20px;"></span>
                <span class="iconPlane"></span>
            </div>

            <!-- Checkbox -->
            <h2 style="margin-top: 80px; margin-bottom: 20px; font-size: 24px; border-bottom: 1px solid #83dbe0; line-height: 2;">Checkbox</h2>
            <span class="cCheckbox">
                <input id="login_keep" class="visuallyhidden" type="checkbox">
                <label for="login_keep" class="cCheckboxLabel">로그인 상태 유지</label>
            </span>

            <!-- Text Color -->
            <h2 style="margin-top: 80px; margin-bottom: 20px; font-size: 24px; border-bottom: 1px solid #83dbe0; line-height: 2;">Color</h2>
            <h3 style="margin-bottom: 1em; font-weight: 500; color: #222;">* colorPrimary</h3>
            <p class="colorPrimary">Lorem ipsum dolor sit amet consectetur adipisicing elit. Facere, debitis. Debitis
                sequi ipsum voluptatibus at nesciunt blanditiis facere, libero, maiores, enim aspernatur officia nulla
                vitae quis adipisci sed laudantium aperiam.</p>

            <!-- InputBox -->
            <h2 style="margin-top: 80px; margin-bottom: 20px; font-size: 24px; border-bottom: 1px solid #83dbe0; line-height: 2;">InputBox</h2>
            <p class="inputField">
                <label for="confirmEmail" class="visuallyhidden">이메일 주소</label>
                <span class="input">
                    <input type="text" id="confirmEmail" placeholder="이메일 주소 입력" value="">
                </span>
                <strong class="message">사용할 수 없습니다.</strong>
            </p>

            <p class="inputField invalid">
                <label for="confirmEmail" class="visuallyhidden">이메일 주소</label>
                <span class="input">
                    <input type="text" id="confirmEmail" placeholder="이메일 주소 입력" value="">
                </span>
                <strong class="message">사용할 수 없습니다.</strong>
            </p>

            <p class="inputField valid">
                <label for="confirmEmail" class="visuallyhidden">이메일 주소</label>
                <span class="input">
                    <input type="text" id="confirmEmail" placeholder="이메일 주소 입력" value="">
                </span>
                <strong class="message">사용할 수 있습니다.</strong>
            </p>

            <!-- Button -->
            <h2 style="margin-top: 80px; margin-bottom: 20px; font-size: 24px; border-bottom: 1px solid #83dbe0; line-height: 2;">Button</h2>
            <div>
                <button type="button" class="cBtn">Button style1</button>
                <button type="button" class="cBtn line">Button style1</button>
                <a href="#" class="cBtn">a Button</a>
                <div style="height: 20px;"></div>
                <button type="button" class="cBtn2">Button style1</button>
                <a href="#" class="cBtn2">a Button</a>
                <button type="button" class="cBtn2 icon">Button icon</button>
                <a href="#" class="cBtn2 small">a Button small</a>
                <button type="button" class="cBtn2 small">Button small</button>
                <button type="button" class="cBtn2 fill">Button small</button>

                <h3>Button group</h3>
                <div class="writeButtons rowGroup">
                    <button class="cBtn line">취소</button>
                    <button class="cBtn">확인</button>
                </div>
            </div>

            <!-- Popup -->
            <h2 style="margin-top: 80px; margin-bottom: 20px; font-size: 24px; border-bottom: 1px solid #83dbe0; line-height: 2;">Popup</h2>
            <button class="cBtn jsExample">Open Popup</button>
            <div class="cPopup popupExample">
                <div class="cPopupInner">
                    <h2 class="cPopupTitle">계정찾기</h2>
                    <button type="button" class="cPopupClose jsPopupClose" aria-label="Close this popup">
                        <span class="btnX"></span>
                    </button>
                    <p class="cPopupDesc">
                        연동하신 SNS 계정을 잊으셨나요?<br>
                        Chatbit 회원가입 시 등록한 이메일 주소를 입력해 주세요.
                    </p>
                </div>
            </div>
        </div>
    </div>

    <jsp:include page="/chatbit_m/include/footer.jsp" flush="true"></jsp:include>

    <script>
        $('.jsExample').click(function () {
            $('.popupExample').fadeIn(200);
        });
    </script>

</body>

</html>